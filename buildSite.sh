#!/bin/bash

set -euo pipefail

GIT_ROOT=$(git rev-parse --show-toplevel)

cd "${GIT_ROOT}"

function buildFile {
  local fname="${1}"
  if [ ! -f "src/${fname}.md" ]; then
    echo "WARNING: missing file ${fname}"
  else
    echo "Building file ${fname}"
    pandoc -f markdown \
      -t html \
      -o "public/${fname}.html" \
      -s \
      --variable colorlinks=true \
      --css style.css \
      --embed-resources \
      --template=template.html5 \
      "src/${fname}.md"
  fi
}

buildFile "auditions"
buildFile "characters"
buildFile "plot-summary"
buildFile "script-cuts"
buildFile "script"
buildFile "score"
buildFile "lyrics/gwendolyn"
buildFile "lyrics/bernadette"
buildFile "lyrics/jax"
buildFile "lyrics/you-never-can-tell"
buildFile "lyrics/when-youre-a-bard"
