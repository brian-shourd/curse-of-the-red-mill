---
title: "Curse of the Red Mill Auditions"
subtitle: "An operetta for adventurers levels 4-6"
---

If you're here, you must be interested in auditioning for the Curse of the Red Mill, an upcoming musical at Enoch City Arts. Thank you for your interest! We're excited to have you be a part of this!

## Key Info

- Audition dates:
  - Tue, July 16: 6pm
  - Fri, July 19: 6pm
  - [Click to sign up for a time slot](https://m.signupgenius.com/#!/showSignUp/10C0E4DA5AD2CA1FFC61-50230602-auditions)
- Rehearsal dates:
  - Sat, July 20: 3:30pm-6:30pm (first read-through)
  - Every T/W/Th/F, July 30 - Sep 26: 6:30pm-9:30pm
- Performance dates: (10 performances)
  - Friday night, Saturday matinee, Saturday night
  - Sep 27/28, Oct 4/5, Oct 11/12
  - Bonus Thursday night Oct 3
- [About the show](#about-the-show)
- [Role information](/characters.html)
- Audition requirements:
  - [1 "audition cut" from Curse of the Red Mill](#audition-cuts)
    - We will provide backing music for these
  - (optional) A second song of your choice
    - Please bring your own backing music. We will have speakers that you can connect a device to and a CD player.
    - If it is a second song from Curse of the Red Mill, we will provide the backing music
- [What to expect at the audition](#what-to-expect)
- [Everyone is welcome!](#who-can-audition)
- [Who is involved](#who-is-involved)
- Compensation: none

## About the Show {#about-the-show}

"The Curse of the Red Mill" is an adaptation of the 1906 musical "The Red Mill" by Victor Herbert and Henry Blossom. The music is the same, but the characters and plot are heavily modified, and almost all of the words are brand new. Old music, new story.

In this version, an all-bard Dungeons and Dragons game goes off the rails when the players would rather prevent a wedding than fight monsters. It is a geeky comedy celebrating music, tabletop gaming, and friendship.

It is not necessary to know any more than that to audition, but if you are interested, you can read more:

- [Character breakdown](/characters.html)
- [Plot summary](/plot-summary.html)
- [Full script](/script.html)

Please note that the script is subject to change as we go. Since this is an adaptation being done in-house, we are likely to shape the script and score to match what we learn as we rehearse and improvise.

## Learn the Music (Audition Cuts) {#audition-cuts}

Because this is a brand-new set of lyrics, there aren't any existing recordings of these songs being performed.

Instead, we have sheet music and synthesized audio. Each song has two versions - one with the "vocal" parts ("leads"), and one without the lead parts ("backing"). The "vocal" parts don't have sung lyrics, just a synthesized "aah" voice playing the rhythm and notes. You can figure out how the lyrics fit with the notes by reading the lyrics or looking at the sheet music. If this is giving you problems, please [reach out to Brian Shourd (music director)](#who-is-involved) and he'll be happy to help you out!

During the audition, we'll use the "backing" track without the lead parts - the other version is to help you learn the song and hear the melody as you practice.

These 5 songs have been picked and edited down to short audition cuts. Please choose one to learn and sing for your audition. The broad vocal range of each song is listed (Soprano, Alto, Tenor, Bass), but feel free to sing any song in another octave if you prefer.

All of these files (audio and sheet music) can be found in [this Dropbox folder](https://www.dropbox.com/scl/fo/9fyz6gmsyv7n75nju9dde/ACYKyHo8Hz-ZKizL--btLTk?rlkey=csf550h8t2783pbgikwcc432l&st=nim7t7tt&dl=0). Or, if you prefer, the songs can be found in [this SoundCloud playlist](https://soundcloud.com/wonder-otter-underwater/sets/curse-of-the-red-mill-audition/s-QKnDAErxVDT?si=557c7657ff564c1c93fac5c28a59212a&utm_source=clipboard&utm_medium=text&utm_campaign=social_sharing).

- "Gwendolyn" (Soprano)
  - Sung by Gwen
  - [Lyrics](/lyrics/gwendolyn.html)
  - [Sheet music](/music/gwendolyn.pdf)
  - [Leads track](https://www.dropbox.com/scl/fi/9pod8c6hy34d08lpvifuj/Gwendolyn-audition-cut-leads.mp3?rlkey=zraua1t9wc9sieb8zz9j58gho&st=zootpzby&dl=0)
  - [Backing track](https://www.dropbox.com/scl/fi/2b3hyhy15vdu1o3bs6lgu/Gwendolyn-audition-cut-backing.mp3?rlkey=z7zkb572cin3ofy3hnbtpnm6t&st=2gn970id&dl=0)
- "Bernadette" (Alto)
  - Sung by Bernadette
  - [Lyrics](/lyrics/bernadette.html)
  - [Sheet music](/music/bernadette.pdf)
  - [Leads track](https://www.dropbox.com/scl/fi/p08z4q33w52mszblr5eqb/Bernadette-audition-cut-leads.mp3?rlkey=najs3o8de02vxbmkrugrg9cdk&st=fqiust7i&dl=0)
  - [Backing track](https://www.dropbox.com/scl/fi/fjz01vzyc64r988q4wnmg/Bernadette-audition-cut-backing.mp3?rlkey=utm0bbpi5veceq6uc9ufquogl&st=pqc14khc&dl=0)
- "Jax" (Tenor)
  - Sung by Jax
  - [Lyrics](/lyrics/jax.html)
  - [Sheet music](/music/jax.pdf)
  - [Leads track](https://www.dropbox.com/scl/fi/2my3jcdqai8v1ji5vpnra/Jax-audition-cut-leads.mp3?rlkey=rgiegn7u4rat84bi0il7d2yck&st=c0u15nx0&dl=0)
  - [Backing track](https://www.dropbox.com/scl/fi/vyk5uxr83e4kd1bisuqzq/Jax-audition-cut-backing.mp3?rlkey=nhmgtleo37qrghfziil5umw4i&st=jtay79ht&dl=0)
- "You Never Can Tell" (Bass)
  - Sung by GM
  - [Lyrics](/lyrics/you-never-can-tell.html)
  - [Sheet music](/music/you-never-can-tell.pdf)
  - [Leads track](https://www.dropbox.com/scl/fi/br9pagdx2x8fmhgztutgp/You-Never-Can-Tell-audition-cut-leads.mp3?rlkey=9950k6nvy3k5amqxr9kyu9zk5&st=p2el4kk4&dl=0)
  - [Backing track](https://www.dropbox.com/scl/fi/0x93tzu86uhy8cirt688f/You-Never-Can-Tell-audition-cut-backing.mp3?rlkey=ellp2nfbdqohgmu3tuaea12fo&st=816v23xv&dl=0)
- "When You're A Bard" (Any)
  - As written, this is in an Alto/Soprano range, but can be sung an octave lower
  - Sung by Gwen, Bernadette, and Jax
  - [Lyrics](/lyrics/when-youre-a-bard.html)
  - [Sheet music](/music/when-youre-a-bard.pdf)
  - [Leads track](https://www.dropbox.com/scl/fi/nyqd459ucfxyzn6ytdt0b/When-You-re-A-Bard-audition-cut-leads.mp3?rlkey=pxvhjgo46op9mmb405g1mmikg&st=5upmx1c7&dl=0)
  - [Backing track](https://www.dropbox.com/scl/fi/87dgxb7chxek2gfgx6agw/When-You-re-A-Bard-audition-cut-backing.mp3?rlkey=7svj5qcdld8nntbj9gg66fdrx&st=wkl6xhtg&dl=0)

## What to Expect During the Audition {#what-to-expect}

We'll ask you to fill out an audition form, which will indicate things like your name, contact info, schedule conflicts, preferred roles, singing range, and acting history. If you have an acting resume, feel free to bring that. Please check your schedule before auditioning.

During the audition, we'll ask you to do two things:

1. Sing your prepared cut(s) for the directors
2. Read some lines from the show in groups with other auditioners

If you like, you can [see the lines that we will be reading over during auditions here](/script-cuts.html). No need to memorize or bring them, they are just here for you to read ahead of time if you are interested.

If you have a preferred role, we may have you read for that part and/or other parts. If you have questions or concerns, or need any accomodations, [please let us know beforehand by email,](#who-is-involved) and we'll do our best to help you out.

We will let you know our casting decisions by Saturday morning (July 20), so that you can come to the full read-through on Saturday afternoon (3:30pm-6:30pm).

## Who Can Audition {#who-can-audition}

Anyone! We mean it - we welcome actors from any background and experience level, of any age, gender, ability, race, religion, body type, or ethnicity. We are striving to create an atmosphere of inclusion and inspiration, so please reach out. Whether you've been singing and acting for your whole life or never considered it before this very moment, we would love to see you at auditions!

## Who is Involved {#who-is-involved}

- Kevin Bonner - Director
  - Email: bone_ified@hotmail.com 
- Brian Shourd - Music Director and script adapter
  - Email: brian.shourd@gmail.com
- Bryan Bullock - Dramaturg

## Questions?

Send an email to Kevin Bonner and/or Brian Shourd with any questions that you have about the audition or the show.
