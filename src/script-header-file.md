---
title: Curse of the Red Mill
subtitle: An Operetta for Adventurers Levels 4-6
toc-title: Table of Contents
date: 2024-07-01
version: 4
---

This is the preliminary and still-in-progress script for the Curse of the Red Mill. This copy is intended to give an overview of the play for curious folks considering an audition. The actual words are subject to change, before and during rehearsal.

## Conventions

The story takes place on two levels. In the framing story, we have the "real life game group", where four modern-day people (GM, Gwen's player, Bernadette's player, and Jax's player) are gathered around a table to play a game of Dungeons and Dragons. The inner story is the game that they are playing, which we see acted out on stage, though it takes place in the game group's collective imagination.

Throughout the script, the following terminology is used:

- IC (in character): meaning "within the fictional world of the game"
- OOC (out of character): meaning "outside of the fictional world of the game", where the players speak to each other or to the GM
- GM (game master): the person who controls the fictional world of the game
- Players: the three members of the game group playing Gwen, Bernadette, and Jax
- PC (player character): the three characters played by the players (Gwen, Bernadette, and Jax)
- NPC (non-player character) - any character who is being controlled by the GM, rather than one of the players. This includes every character in the game other than Gwen, Bernadette, or Jax.


