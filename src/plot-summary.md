---
title: "Curse of the Red Mill"
---

## Plot Summary

In "The Curse of the Red Mill", there are two stories happening, both of which are comedies. The framing story at the table is that of a dysfunctional tabletop RPG group (playing Dungeons and Dragons). The Game Master (GM) has brought a module called "The Curse of the Red Mill", and wants a no-nonsense dungeon crawl. Unfortunately, he has recruited three players who are much more into the idea of character drama and hijinks than of fighting monsters. Naturally, the players have all picked bards (Gwendolyn, Bernadette, and Jax).

The in-game story is a story of friendship, adventure, and of forging one's own path. It takes place in a small coastal fantasy town containing a legendary windmill (the eponymous Red Mill) which is said to be inhabited by a fearsome beast. Adventurers come from all over to slay the beast, but none have ever returned.

By tradition, they stay at the inn, where Gwen works and loves to hear tales. It is owned by her beloved aunt, Elandra. Gwen's mother, the sheriff of the town, has arranged for her to be married to a local baron tomorrow, against Gwen's desire to remain unattached. Out of options, she has asked her lifelong friend Captain Bernadette to take her away on her ship, and Bernadette is only too happy to agree. But Jax, who has been freeloading at the inn for a week, accidentally spoils their plans mid-flight. The sheriff throws Gwen in jail to await her wedding.

Working with Elandra, Jax and Bernadette are able to break Gwen out of jail, but cannot leave town because Bernadette's ship has been seized. The three plan to hide out in the Red Mill until after the wedding. Inside, they disable, sidestep, or otherwise work around every obstacle the GM gives them, easily making it to the beast's lair. They discover that the legendary beast is actually Elandra, cursed to transform each night until she finds true love. Gwen breaks the spell by appealing to the familial love that they share.

The players celebrate their victory, but the GM sees this as a personal failure, because the players didn't really "do" the dungeon or the boss fight. The players are having a great time, though, and ask the GM to stay and keep playing, so that they can figure out how to stop the wedding and get Gwen out of town.

Come morning, Jax and Bernadette disguise themselves as detectives (Sherlock Holmes and Dr. Watson) to help the sheriff investigate Gwen's disappearance. They discover that the sheriff plans to destroy Bernadette's ship, and convince her to wait until after the wedding. Bernadette pretends to interview the baron for clues, but she sings a pleading love song and convinces him to forsake his bride-to-be and marry her instead. He gives her a necklace as proof of his commitment.

Now that the baron plans to marry Watson, Gwen realizes that she is no longer forced to leave town just to get away from the wedding. She could stay and go back to normal life. Given the choice, Gwen decides that she wasn't leaving because she had to; she actually wants to get away, but must give herself permission to go. She decides to confront her mother and concocts a plan.

As the guests are arriving for the wedding and sitting down, "Holmes" and "Watson" bring Gwen back to the sheriff just in time to walk down the aisle, collecting the reward for locating her. Before the wedding can commence, the baron reveals that he has fallen in love with Dr. Watson and will marry her instead. He gives Gwen a ring by way of apology. Gwen, now free of her arranged marriage, joyfully steps onto Bernadette's ship where Jax, Bernadette, and Elandra have been preparing to sail. 

The sheriff makes one final attempt to stop Gwen, just as she did before, by threatening her friends and posing an ultimatum. This time, though, Gwen calls her bluff and asserts her own independence, guilt-free. They leave behind the Red Mill forever, somewhat richer for their troubles.

The RPG group decides that they should make this one-shot into an ongoing campaign, and begins collaborating on their next adventure together.
