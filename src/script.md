---
title: Curse of the Red Mill
subtitle: An Operetta for Adventurers Levels 4-6
toc-title: Table of Contents
date: 2024-07-01
version: 4
---

This is the preliminary and still-in-progress script for the Curse of the Red Mill. This copy is intended to give an overview of the play for curious folks considering an audition. The actual words are subject to change, before and during rehearsal.

## Conventions

The story takes place on two levels. In the framing story, we have the "real life game group", where four modern-day people (GM, Gwen's player, Bernadette's player, and Jax's player) are gathered around a table to play a game of Dungeons and Dragons. The inner story is the game that they are playing, which we see acted out on stage, though it takes place in the game group's collective imagination.

Throughout the script, the following terminology is used:

- IC (in character): meaning "within the fictional world of the game"
- OOC (out of character): meaning "outside of the fictional world of the game", where the players speak to each other or to the GM
- GM (game master): the person who controls the fictional world of the game
- Players: the three members of the game group playing Gwen, Bernadette, and Jax
- PC (player character): the three characters played by the players (Gwen, Bernadette, and Jax)
- NPC (non-player character) - any character who is being controlled by the GM, rather than one of the players. This includes every character in the game other than Gwen, Bernadette, or Jax.



<div class="act script-start"></div>

# Act 1

*(Curtain rises on a stage-upon-a-stage. The smaller stage is a step-height platform as large as possible, where all "in character" action takes place. It is decorated like a battle mat, with a square or hexagonal grid. It represents the table around which the players are playing. There are four seats surrounding the table - three together for the players and one apart for the GM, who sits behind a GM screen. On the table in reach of the GM's seat is a large 20-sided die which can be rolled, but whose numbers the audience cannot read.)*

## 1 - 'Round This Table

| *(PLAYERS)*
| Round this table we will
| Gather dice for a thrill-
| -ing adventure game
| We've brought character sheets
| And we're taking our seats
| Let us play, play, play

| There's a story to tell
| And a dragon to fell,
| A foreboding foretell-
| -ing, a dark citadel
| We'll be casting a spell
| And have treasure to sell
| By the end we'll all yell
| "We love this"

| *(GM)*
| Friends, as you know we all have our roles to play
| In our game today as we navigate rules meant to shape our play
| So, though I know that you're eager to begin
| You should grab a pen, and take notes as I walk through our system

| *(PLAYERS)*
| Though we've all had our fill
| Of your lectures we still
| Want to play this game
| We've brought character sheets
| And we've taken our seats
| Let us play, play, play

| *(GM)* But the rules,
| *(PLAYERS)* We're not fools, no more rules,
| We're not fools, we're not fools
| No more rules, no more rules!

| Enough real life let us have a little pleasure now
| For we have waited long and we're really very hungry
| For adventure 
| If we need help we will ask and you can show us how
| We'll learn it as we go
| And we want to start right now

| *(GM)*
| But there are broke-
| -en rules I am home-brewing
| It would be nice
| If you could read the dice

| *(PLAYERS)* But can't we just explain what we are doing?
| *(GM)* I cannot trust it won't be your undoing
| *(PLAYERS)* We know it might
| *(GM)* It isn't right
| *(PLAYERS)* We're ready now
| *(GM)* Don't make a row
| *(PLAYERS)* We're ready now
| We're ready, ready, ready

| *(Simultaneously)*
| *(PLAYERS)* Enough real life let us have a little pleasure now
| *(GM)* It would be nice if you all could read the dice
| *(PLAYERS)* For we have waited long and we're really very hungry
| *(GM)* Without the rules, we cannot have
| *(ALL)* Adventure!

| *(PLAYERS)* Enough real life, enough of rules
| *(GM)* I suppose it's fine
| *(ALL)* Let's have a little pleasure now
| *(PLAYERS)* You'll show us how
| *(GM)* I'll show you how
| *(ALL)* So that we all can start right now

| *(ALL)*
| Let us have a little fun, let us have a little fun
| We're ready now
| Let us have a little fun, let us have a little fun

| *(PLAYERS)* Come on!
| *(GM)* Ok! Enough of rules
| *(PLAYERS)* Let us have a little fun!
| *(GM)* You'll learn the rules
| *(PLAYERS)* Let us have a little fun!
| *(ALL)* We're ready now so let us have a little fun, fun, fun!
| Fun, fun!

*(Throughout this scene, GWEN is making googly eyes at the GM. The GM is oblivious to GWEN's interest. BERNADETTE is trying to figure out what GWEN sees in him. JAX is on their phone most of the time)*

**GM:** Ok, ok. If you really can't stand it, we'll skip the rules explanation-

**ALL PLAYERS:** *(sighs of relief)*

**GM:** *(to BERNADETTE)* but you're new - at least tell me that you know how the game more-or-less works.

**BERNADETTE:** I understand it's like "Whose Line is it Anyway" - everything is made up. Except it appears that the experience points matter? And someone sent me a spreadsheet?

**GM:** You didn't like the spreadsheet I made? I know it can be intimidating but it's supposed to help with character creation.

**GWEN:** I liked it!

**GM:** Thank you, I worked really hard on the macros, did you like -

**BERNADETTE:** Did you know; I actually met Drew Carey once? My improv troupe was in the audience of a live taping of "Whose Line". It was a dream come true! A chance to learn and study from some true masters of the craft.

*(No one is impressed)*

**GM:** Ok, well, it's not really like that, no. It's more like "playing pretend", but with some extra rules and procedures to handle common situations. You each have a character that you control on your sheet there-

*(GM looks to each player in turn)*

**GWEN:** Gwendolyn

**BERNADETTE:** Bernadette

**JAX:** *(making it up on the spot)* uh, Jax?

**GM:** Is that a question?

**JAX:** Oh... no that's just how it's pronounced. It's lilting. Elvish, you know?

**GM:** So, Jax?

**JAX:** Aw, you're butchering it! My ancestors are turning in their graves! Nevermind, we'll skip the accent and go with just Jax.

**GM:** Just Jax?

**JAX:** I said skip the accent!

**GM:** Jax, don't be a-

**JAX:** Stop. Nailed it.

**GM:** *(starts to retort, decides to leave it alone)* As I was saying, you each control a character, and you just tell me what they do. You have total control over your character's thoughts and actions.

*(JAX goes back to their phone, disinterested)*

**BERNADETTE:** *(to GM)* So what character are you playing?

**GM:** Actually, I play lots of characters. You each only have one, but I have to control every other character that you might interact with. And I describe the scene and tell you what else is going on. You might say that I breathe life into the world, like a benevolent god.

**GWEN:** Mmm-hmm, you're the boss.

**GM:** That's right.

*(BERNADETTE shares a look with GWEN and mouths "Really? This guy?")*

Also, since I'm the only one who knows the rules, I'll have to enforce those, too. I'll tell you when to roll dice and stuff like that. *(to BERNADETTE)* Any more questions, or was that clear?

**BERNADETTE:** Crystal. Gwen asked me to join because I do improv, so this will be simple.

**GM:** Yeah, well, I'm still not sure they're that similar...

**GWEN:** Sure they are! We're going to work together to tell a story about mysterious strangers, -

**BERNADETTE:** daring heroics?

**GWEN:** family tragedy,

**BERNADETTE:** swashbuckling swordfights,

**GWEN:** impossible choices,

**JAX:** and then we'll light something on fire! *(JAX waits for a reaction and gets none)* That's how it usually goes.

**GM:** Well, mostly, we're going to explore this awesome dungeon that I brought. With monsters and traps and secrets and treasure!

**BERNADETTE:** Now hang on! I thought that we could choose what our characters do. Right?

**GM:** Absolutely! How you explore my dungeon is entirely up to you.

**JAX:** I wish! Last time, you said that my dragon mating call whistle was OP and I couldn't use it to clear the dungeon!

*(JAX and GM descend into heated arguing. BERNADETTE pulls GWEN to the side)*

**GM:** You were treating that dragon like a personal flamethrower! It was ruining the whole session!

*(Lights on GM/JAX fade and argument becomes inaudible. GWEN and BERNADETTE speak conspiratorially, as a side conversation.)*

**BERNADETTE:** Tell me honestly - look me in the eyes - has this whole thing been an elaborate prank? This is the guy?

**GWEN:** This is the guy! I think he's just exuding this... aura. You know the phrase "big dice energy"?

**BERNADETTE:** Please never say that again.

**GWEN:** Well, whatever it is, he's got it.

*(GWEN and BERNADETTE look to GM. Lights come up on GM/JAX still arguing)*

**GM:** Just to be clear, we're using rules-as-written, not rules-as-... stupid.

*(return to inaudible, lights down)*

**BERNADETTE:** Look, I love you, but I don't know if I can survive a whole night with this guy. I think I might have an aneurysm.

**GWEN:** Come on, you said you'd be my wing woman. It's just one game. It's fun!

**BERNADETTE:** Fun? *(pointing to JAX)* And what about them? Lighting something on fire is a classic rookie mistake in improv; someone gets stumped on what to do next, so they jump to random violence.

*(lights up on JAX and GM)*

**JAX:** *(overhearing)* Rookie? You're the noob!

**GM:** Wait, guys...

*(JAX and BERNADETTE ignore the GM, who falls into despair at another game gone bad, even before it could start. GWEN notices the GM and starts shuffling around in her bag)*

**BERNADETTE:** Noob? I didn't think people actually said that in real life.

**JAX:** I guess everyone else just muttered it under their breath, and you were too self-absorbed to notice.

**GM:** *(to self)* This is even worse than last time...

**BERNADETTE:** Well excuse me for trying to help you all with some improv tips!

**JAX:** Oh! Do you do improv? I had no idea!

**GWEN:** *(brandishing a CD)* Wait, wait! I almost forgot! I brought a new player, so I get to pick the soundtrack today, right?

**GM:** What? Oh, yeah, that is the rule, isn't it? Ok, fine, you can be the DJ tonight. But it better be epic!

**GWEN:** Oh, it'll be perfect! Just what we need to give this game a little pizzaz! It's got something for everyone - a little adventure, some romance. *(to GM)* I made it just for you.

**GM:** *(oblivious)* For me? I don't like romantic music. Anyway, before we get sidetracked again, I did ask you all to come up with character backstories and reasons that they come here, to the inn near the Red Mill.

**BERNADETTE:** What's the-

**GM:** *(hamming it up)* What's the Red Mill, you ask?

*(As GM speaks, CHORUS NPCs and ELANDRA enter and set the stage for the inn. They remain on stage as inn patrons, seating at tables or the bar, silently ordering drinks, conversing, or playing dice.)*

Well, for years the famous Red Mill of this town has been plagued by a legendary and monstrous Beast, and adventurers from around the world travel to test their might against it. Many have tried, and many have died, seeking the heart of the cursed creature who stalks the tower of the mill each night. And by tradition, these brave souls all spend a night or two here, at this inn, where the innkeeper Elandra hears their tales of past glories before they go to meet their fate against the beast.

Let's introduce-

**BERNADETTE:** Who's at the inn right now?

**GM:** What?

**BERNADETTE:** You're setting the scene, right? Who all is there?

**GM:** I don't know, some villagers. It's not important.

**BERNADETTE:** Yes, it is. *(pointing at an NPC)* Who's that? *(teasing GWEN)* He's got a nice aura.

*(MARVIN mouths to GM "what's my name?")*

**GM:** Uh, that's, uh, Mar...vin. Marvin.

**NPCs:** *(raising hands in celebration)* Marvin!

**BERNADETTE:** What's he do?

**GM:** He's a.... milliner.

**BERNADETTE:** Marvin the Marvelous Milliner?

**JAX:** He works in the cursed mill?

**GWEN:** No, that's a miller.

**JAX:** But Marvin's a...?

**GM:** Milliner.

*(beat, JAX is puzzled)*

**BERNADETTE:** You know, a modiste?

*(beat)*

**GWEN:** Like a haberdasher.

**JAX:** Did you three go equal shares on a word-of-the-day calendar? What are you talking about?

**GM:** A hatter. He makes hats.

**JAX:** Just say that, then!

**BERNADETTE:** Why does this little village need a dedicated milliner? Do they all wear elaborate hats?

*(simultaneously)*
**GM:** No. **JAX:** Yes!

**JAX:** Too late, I'm imagining all of them with exquisite headpieces now!

**GM:** But that's goofy! It'll ruin the atmosphere!

**BERNADETTE:** I kinda like it.

**GWEN:** Me too.

*(All NPCs pull out unlikely hats concealed nearby and don them. For the remainder of the show, every NPC wears a hat.)*

**GM:** Whatever, it doesn't matter anyway. Now before we get even more distracted, can we please introduce your characters and their backstories? Briefly? Gwendolyn, start us off.

**GWEN:** Yes, sir!

*(GWEN hits play on the boom box. GM groans as the music starts)*


## 2 - Gwendolyn

| It seems, in my family, a truth is assumed
| That each woman's born lacking a husband
| So all of my life I've been polished and groomed
| Always knowing I'm bound for a dead end

| My mother the sheriff's tomorrow arranged
| The baron will wed me and I'll be exchanged
| A new family name, but the rest is unchanged
| And each one believes I am theirs

| But I'm planning a life just for me
| I'll not be a wife or lady
| From daughter to bride, never Gwen on my own
| The path that's before me seems written in stone

| Yes tomorrow my wedding arrives
| It's the future my mother contrives
| A sham baroness bound by corset and dress
| But this woman has plans

**GM:** This is the soundtrack you picked?

**BERNADETTE:** Were you expecting "Lady Marmalade"? *(BERNADETTE laughs at her own joke, but nobody else gets it)* Moulin Rouge? Red Mill? Nevermind.

**GM:** No, I mean - where are the battles? The pounding drums that foretell the coming of the orcish hordes? This is all wrong.

**GWEN:** I mean, I just thought that it would fit with my character, since she's a bard.

**GM:** A bard? I specifically said no bards!

**GWEN:** You did? Oh I'm sorry! Maybe I can roll up a new character real quick.

**GM:** No, no, there's no time. It's ok, it'll be fine.

*(GWEN pouts)*

Hey, no, really, it's not a big deal. I should have been clearer.

*(GWEN pouts harder)*

Come on, it's all right. Tell me, who are all these other characters from your backstory?

**GWEN:** Well, there's the sheriff of the town, she's my mother. And the baron - I'm supposed to marry him tomorrow. Oh and the innkeeper you mentioned?

**GM:** *(pointing to Elandra)* Elandra?

**NPCs:** *(raising hands in celebration)* Elandra!

**GWEN:** I think that she's my aunt.

**GM:** Your aunt? No, that won't... but, well... hmm... *(smiling)* ok, yeah, that can work I guess. Anything else?

**GWEN:** No, I think the rest will come out during play.

**GM:** Good. Let's hear about... Jax, tell me about your character.

*(JAX looks up from phone)*

**BERNADETTE:** Actually, can I go next?

 *(JAX rolls their eyes)*
 
Gwen helped me figure out how this game works, so she and I planned our characters together.

**GM:** Oh yeah, sure, let's hear about... *(checks notes)* Bernadette.

*(During BERNADETTE'S song, JAX steals her character sheet and begins copying over parts of it. Nobody notices this. They put the sheet back before the song ends.)*


## 3 - Bernadette

*(As she sings, BERNADETTE pulls in the NPCs standing around, treating them as an audience and dancing with them as she parades around. She captivates them. By the end, they join in as the CHORUS, echoing her words. Although BERNADETTE's player is insecure and obnoxious, BERNADETTE the character is cool as a cucumber, bold, and charismatic.)*

| I'll tell you all her history
| For there's an air of mystery
| Regarding Bernadette

| She's known upon the sea
| For her nerve and gallantry
| And her rise to captain from cadet

| From port to port all pirates quail
| The sailors drink and tell the tale
| Of her ferocity and skillful bayonet

| But Gwendolyn, she knows
| That where e'er the captain goes
| She has a lifelong friend in Bernadette

| For Bernadette,
| In each vignette
| Can hold her own, you know she needs no epithet
| She vows to never be a wife
| To sail the sea is her whole life
| She takes the helm and wheel to dance a minuet

| But she has yet
| A sole regret
| That she has left behind a friend she won't forget
| She has a ship at her disposal
| And to Gwen makes this proposal
| To set sail tonight with Bernadette

**GM:** That's more -

**BERNADETTE:** *(holding up a hand)* Ah- I'm not done.

| While swashing buckles valiantly
| Her fencing form has pageantry
| Our stunning Bernadette

| But fairness and compassion
| Always stay her hand, mid-action
| When a foe requests a tête-à-tête

| She's frightening, she's bold
| She could be the centerfold
| The odds are always on her side so place your bets

| Each day her legend grows
| It's told in verse and prose
| The people cry out for her: *(CHORUS)* Bernadette!

| *(BERNADETTE)* Yes, Bernadette!
| *(CHORUS)* Yes, Bernadette!
| *(BERNADETTE)* No sobriquet!
| *(CHORUS)* No sobriquet!
| *(BERNADETTE)*  Although I've heard her called the Nautical Coquette
| Attend her briny cabaret
| Her ship Arachne tacks in spray
| Another life she might have been a first vedette!
| *(CHORUS)* My word!

| *(BERNADETTE)* But she has yet
| *(CHORUS)* But she has yet
| *(BERNADETTE)* A sole regret
| *(CHORUS)* A sole regret
| *(BOTH)* That she has left behind a friend she won't forget
| She has a ship at her disposal
| And to Gwen makes this proposal
| To set sail tonight with Bernadette

**GM:** That's more like it! A captain you said... so probably a fighter, or maybe a ranger? I'm pretty sure that I have a swashbuckler subclass around here somewhere...

**BERNADETTE:** I can see how you'd think that, but no, I'm also a bard.

*(GM shoots a look at GWEN)*

**GWEN:** I forgot! Really, I'm sorry!

**GM:** Two bards!?

**GWEN:** Yeah, it seemed right up her alley, and... it just sort of happened. It'll be ok, right? Don't you like that we tied our backstories together and into the town?

**GM:** *(grumbling)* I do like that. My groups don't usually do that kind of thing. It's like pulling teeth trying to get them to even come with a character actually prepped, so looking at it that way, we're off to a good start. Even with two bards. How does your character fit into all of this, Jax?

**JAX:** *(looking up from phone)* Huh?

**GM:** Can you share the backstory that you came up with for your character?

**JAX:** Oh, uh, yeah...


## 4 - Jax

*(CHORUS NPCs help to stage the story of the song while JAX saunters through the scene. JAX moves little; the world revolves around them. By the second dance, JAX is back at the inn, glad handing the locals, picking pockets, and setting up a shell game for the rubes.)*

| Some days you get the worst of luck just wanderin' the town
| Not botherin' nobody only keeping your head down

| You see a friendly neighbor selling jewelry on the cheap
| You make an easy bargain never knowing you're in deep
| Next day the guard appears and then they throw you in the keep

*(During the dance section, JAX is locked up, but switches cuffs with the guard and easily escapes)*

| You say you never stole 'em; do they listen to your pleas?
| It figures that the jewels belonged to some old baron's squeeze

| For breaking out of jail it seems they want to have your head
| And now you're skipping town 'cause of a fear of being dead
| You're holed up in an inn wit' lots of bills and zero bread

**GM:** Is it just me, or do you always play a rogue?

**JAX:** *(smiling)* I hate to disappoint you, but Jax isn't a rogue.

**GM:** Don't even say it! Don't you dare!

**JAX:** They're a bard.

**GM:** What am I going to do with three bards? *(thumbing through the module)*  I guess if I change this stat block... get rid of this trap door, you'll never survive that... Wait! Wasn't Greg going to come tonight? He rolled a barbarian, I thought.

*(CUE MUSIC: "5 - Thrulsnard (Night on Bald Mountain)")*

*(enter THRULSNARD, looking mean and ready to fight.)*

**JAX:** Oh yeah, Thrulsnard the Barbarian! I love that guy!

*(THRULSNARD roars, waves his axe, and looks menacing.)*

**GM:** Yes! A raging battle-hungry man-tank is just what we need to balance this party!

*(MUSIC: end)*

**JAX:** Yeah, no, Greg had to work tonight.

*(NPCs remove THRULSNARD's helmet, replace it with baseball cap with a fast food pizza logo. Another NPC takes his axe and hands him a pizza box. THRULSNARD looks angry but reserved, in the way of anyone who's ever worked in the service industry. Through the next scene, he tries to deliver the pizza to the patrons at the inn. There are no takers, and eventually he quietly exits, pizza in hand)*

**JAX:** Poor Greg.

*(GM, JAX, and GWEN assume poses of mock reverence and sadness, as for a funeral)*

Greg may not be with us, but his character lives on. Always in the background of each scene, waiting for the next session. Thrulsnard, you are not forgotten.

This one's for you, buddy!

*(JAX loudly opens a can of Mountain Dew)*

**BERNADETTE:** *(looking at JAX's character sheet)* You thief! You stole this whole character idea from us!

**JAX:** No I didn't, it's a completely original idea.

**BERNADETTE:** You wrote the same spell list as me - Charm Person, Calm Emotions, Disguise Self, Suggestion, Command - you even put Vicious Mockery on here!

**JAX:** That's because... I like all those spells. Look, I'm just as surprised as you - it turns out that you have good taste! Who knew?

**GM:** Wait! Jax, just to be clear - did you say that the baron is after you?

**JAX:** He was - that's why I skipped town and ended up here.

**GM:** Well, I guess that he's supposed to be here tomorrow for the wedding. That'll be interesting - maybe you'll need to go hide somewhere. Like the Red Mill.

**JAX:** He will?

**BERNADETTE:** You're not even listening! Gwen is supposed to marry the baron tomorrow, that's what she said. Improv is 90% listening.

**JAX:** This isn't improv! And anyway, clearly I _was_ listening. I heard Gwen say whatever she said about the baron and subconsciously put it into my backstory just now.

**GM:** I thought that you came up with that backstory ahead of time like you were supposed to.

**JAX:** *(ignoring the GM)* If the baron's coming here tomorrow, then I guess I'm checking out tonight.

**GM:** Funny, I think that's just what Bernadette and Gwen were thinking. Let's look and see what's happening at the inn.

*(Exit GWEN and BERNADETTE in preparation. As the GM sets the scene, lights up on ELANDRA and some GUESTS of the inn, including MARVIN and BIG JIM. GUESTS and JAX take seats around the outskirts of the stage. ELANDRA is doing chores and serving customers.)*

Where was I? *(mumble)* ...and by tradition these brave souls… no, a little past that…  where the innkeeper, Elandra, hears their tales of past glories before they go to meet their fate against the beast.

*(Enter BERNADETTE, striding to center stage)*

You see Elandra, working tirelessly to keep this inn afloat, cleaning out a mug with a well worn cloth. Without looking up, she says...

**ELANDRA:** Welcome! Come on in I'll be right with you. Can I bring you a drink?

**BERNADETTE:** Elandra? Only a year and already you've forgotten me? Did I make so weak an impression?

**ELANDRA:** Bernadette! You're here just in time! Wait a minute, I'll get Gwen - she's going to be so happy to see you! *(shouting offstage)* Gwen! Come on out, we have a guest!

**BERNADETTE:** So what's this I hear about Gwen getting married? And to a baron? She's doing well for herself!

**ELANDRA:** For herself? It's her mother's scheme, buying a position near power at the low low price of one daughter.

**BERNADETTE:** That's the final straw, it has to be. This time I'm sure she'll cut ties.

**ELANDRA:** *(imitating GM)* "Just to be clear" - you'll take her with you?

**BERNADETTE:** Was there ever any doubt? I've practically begged her to join me on board the Arachne every time we've docked. Not ideal circumstances, but this might be just the wakeup call she needs.

*(Enter NPC with a pirate hat (CUTTER CROWLEY), crashing through the inn's front door and making a scene)*

*(MUSIC CUE: "5A - A Pirate Appears")*

**CROWLEY:** The Beast!! *(gasp)* The Beast of the Red Mill!

*(pause as he waits for a reaction and gets none)*

Killed me whole crew, it did! Slaughtered them! And, aye, it did give me a death blow. Run me through with its bloody claws, it did!

*(CROWLEY looks to GM for guidance, since the players are not interacting. GM gives a "keep going" signal.)*

But it took me maps! The maps what I use to keep track of me treasure! Me hordes... *(coughing)* piles... treasure...*(dies)*

*(End music: "5A - A Pirate Appears")*

**BERNADETTE:** Who was that?

*(MUSIC CUE: "5B - The Pirate King" - from Pirates of Penzance)*

| *(JAX)* Oh, he was the Pirate King!
| *(NPCs)* He was! Hoorah for the -

**GM:** No! We don't have the Forgotten Realms license. And - just to be clear - I name the NPCs. That was the famed pirate Cutter Crowley.

**NPCs:** *(raising hands in celebration)* Cutter Crowley!

**BERNADETTE:** Oh, ok. Anyway... what about you, Elandra? When Gwen and I leave, will you come too?

**GM:** Wait, what about the pirate?

**BERNADETTE:** I'm a pirate hunter, remember? I'm glad he's dead.

**JAX:** Fine, I'll go check it out. *(JAX walks over)* I loot the body.

**GM:** Ok, I'll roll perception for you. What's your modifier?

**JAX:** 2

**GM:** *(rolling)* 2... plus 2 is... 4. You find a piece of parchment, which might be a partial map of the Red Mill, except that it is ruined and stained with blood. Deciphering this will require careful study, or a very high intelligence check.

**JAX:** Gross. Nevermind, let's keep going.

*(Enter GWEN)*

**GWEN:** Bernadette! You came!

*(GWEN runs over to embrace BERNADETTE, casually stepping over CROWLEY)*

**BERNADETTE:** I got your letter! You said to come at once, and as your best and oldest friend, I loosed all sails to be here. I didn't miss the wedding, did I?

**GWEN:** Not yet, but if we hurry we can both miss it! You still have your ship?

**BERNADETTE:** Until my dying day.

*(JAX hears this and surreptitiously slides closer to be able to hear better)*

**GWEN:** Good. *(pausing awkwardly)* Please -

*(GWEN gestures for BERNADETTE to sit with her. They sit. GWEN fidgets nervously, but ELANDRA catches her eye, smiles, and GWEN relaxes. ELANDRA nods and leaves them alone, going to help other patrons at the inn. She directs some patrons to drag CROWLEY off the stage.)*

**GWEN:** I need to ask you for a big favor.

**BERNADETTE:** Is that all? Don't worry, Elandra was just telling me - you only ever needed to ask.

**GWEN:** Really?

**BERNADETTE:** Don't act so surprised! What are world-renowned sailing friends for? It'll be nice to have a new face on the ship.

**GWEN:** *(nervous and a little sickened)* Oh. So... do you need any... special supplies?

**BERNADETTE:** Just the usual provisions. It won't hold us up any, if that's what you're asking.

**GWEN:** I mean, for the *(lowers voice conspiratorially)* kidnapping?

**BERNADETTE:** *(puzzled)* You want to stage it as a kidnapping? I don't know, that sounds messy...

**GWEN:** *(horrified)* You weren't planning to kill him, were you?

**BERNADETTE:** Woah, woah, woah - you mean the baron!? What kind of rumors are they spreading about me around here? I'm not killing or kidnapping anyone - definitely not a baron! I thought that you were finally going to come adventuring with me, like we've always dreamed!

**GWEN:** But you know I can't do that! If I left, I'd never hear the end of it from my mother!

**BERNADETTE:** Luckily, she's not invited!

**JAX:** Pardon me; I couldn't help but overhear your little plan to, uh... nobble a noble?

**BERNADETTE:** No! I can't stress this enough, we are not planning to kidnap anyone.

**JAX:** *(winks)* Right. *(Loudly, for the room)* That was just a bit of humor! We all know that abducting royalty is illegal, immoral, and *(return to normal volume)* incredibly difficult, which is why it is your good fortune to have an expert on hand.

**GWEN:** You're an expert... at kidnapping?

**JAX:** The industry term is "forceful chauffeur". For a few gold coins and a ride out of town, I can have that baron boy bundled with a bow. What do you say?

**BERNADETTE:** I say that this conversation could get us all hanged, and I preferred when it was private.

**JAX:** Say no more, say no more. I never butt in where I'm not wanted. When you do need my services, here's my card.

*(JAX slides over  a slip of parchment. BERNADETTE picks it up.)*

**BERNADETTE:** This is a warrant.

**JAX:** My bona fides. Now, if you'll excuse me... *(Turning away)* Marvin, you happy hatter, fancy seeing you here! Up for a friendly game of dice? *(JAX sidles up to MARVIN and another NPC (BIG JIM) at a table, out of the light, and their conversation turns inaudible)*

*(As BERNADETTE turns to resume the conversation with GWEN, they are interrupted by another NPC)*

**NPC:** That Beast of the Red Mill is a plague on our town! You two look capable, can't you rid us of this horrible-

**GWEN:** Not now! We're in the middle of a conversation!

*(NPC wanders off)*

**BERNADETTE:** Gwen, why do you make this so hard on yourself? I know that you want to get away from this *(gestures to NPC)* banality and see the world. You can't get enough of my stories. Come live them.

**GWEN:** But my mother...

**BERNADETTE:** makes her own choices. So should you.

**GWEN:** And my aunt? I couldn't leave her behind.

*(CUE MUSIC: "6 - Adventure of Our Dreams")*

**BERNADETTE:** Then bring her too! Stop making excuses. It'll be an adventure!


## 6 - Adventure of Our Dreams

| *(GWEN)*
| My whole life feels like it's ending
| All my options running out
| With my wedding day impending
| And my clouded heart in doubt

| *(BERNADETTE)*
| In my travels I've seen resplendence
| Yet returned every year to this inn
| Just awaiting your independence
| Now let's take this world for a spin!

| *(BOTH)*
| An adventure beyond any other
| Overcoming our sorrow and pain
| Every trouble and care we will banish from there
| 'Till all is made happy again

| When we tire we will lean on each other
| And soon laughter will flow like a stream
| As we sail o'er the sea
| *(BERNADETTE)* me and you?
| *(GWEN)* you and
| *(BOTH)* me,
| The adventure of our dreams

**GWEN:** You're right, Bernadette. Setting sail with you is the best option left to me.

**BERNADETTE:** That makes it sound almost like a kidnapping again. But even if it isn't your first choice, I think that you'll come around to life at sea.

*(ELANDRA walks past, and GWEN pulls her in)*

**GWEN:** Aunty El, did you hear? Bernadette has room on her ship for us both!

**ELANDRA:** Oh Gwen... I'm sorry, but no. My place is here.

**GWEN:** What? But you hate it here! You don't have any family besides me and mother, and she treats you like garbage!

**ELANDRA:** Even still, I have to stay.

**GWEN:** *(to GM)* But I want her to come, too? Can't I convince her with a Persuasion roll?

**GM:** Well...

**GWEN:** *(rolling)* 23! That has to work, right?

**GM:** Umm... no, not quite enough. She says...

**ELANDRA:** My reasons are my own, Gwen. Some problems you can't just run away from.

**GWEN:** And some problems you can *only* run away from! How can I leave if you remain?

**ELANDRA:** Gwen, I've done my best to help you grow, so that you might be strong enough to leave the path that your mother has laid out for you. Nothing would make me happier than to see you take that first step, and to imagine where that journey will take you.

**GWEN:** I'll miss you terribly.

**ELANDRA:** Then come back to visit when you can. I do run an inn, you know.

*(PLAYERS return to OOC)*

**GM:** Ok, so - this is all very interesting, but remember how I brought this module, about the monster in the Red Mill? Doesn't anybody want to fight that, get the treasure? You're not just going to leave those plot hooks dangling, are you?

*(PLAYERS shake their heads)*

**JAX:** There's a whole mess of trouble in the shape of a grumpy old baron coming down on us already. You think we want to add "battle a legendary killing machine" onto that?

**GM:** But I already planned it all out.

**JAX:** Sorry, my friend, but it's like our pal Big Jim always says: you gotta -

**BERNADETTE:** Who's "Big Jim"?

**JAX:** *(pointing to an unnamed NPC)* That guy.

**NPCs:** *(raising hands in celebration again)* Big Jim!

**GM:** Hey! You can't name my NPCs!

*(All NPCs look sad at this "unnaming", start muttering "Big Jim")*

Oh fine, just this once.

**NPCs:** *(raising hands in celebration again)* Big Jim!

**JAX:** Anyway, it's like Big Jim always says:

*(unison)*
**JAX:** you gotta go while the goin' is good!
**BIG JIM:** you gotta go while the goin' is good!


## 7 - Go While the Goin is Good

| *(JAX)*
| Big Jim was a sportin' individual
| A regular gamblin' man
| And if you played with him, he did you all
| As only a gambler can

| Cuz Jim, he always got his money
| On a system that he played
| His explanation may be funny,
| But I'll tell you what he said

| *(JAX/BIG JIM)*
| You can't control the dice
| So just take this advice

| Always go while the goin' is good
| Don't wait to even say "Adieu!"
| For if you stay there it's only one way
| They're bound to get a read on you
| That a roguish gent wears a welcome out quickly
| Is a fact that is understood
| So make your plan to get out while you can
| And go while the goin' is good.

| *(spoken)*
| *(BIG JIM)* Funny you mention that, Jax, because I actually just talked to the big guy.
| *(JAX)* You don't say.
| *(BIG JIM)* I do! And he authorized some odds I never heard the like of before. I'm talking 50:1 - 50 to 1! - odds for anyone what can waltz inna that mill and come back with the beast's head. Whaddya say?

| *(JAX)*
| Big Jim felt the stir of a conviction
| He could beat the dice gods too
| Right here he found it was a fiction
| And his system would hardly do

| *(BIG JIM)*
| But Jax, I'm telling you, it's easy
| Don't be scared, I ain't steerin' you wrong
| *(JAX)*
| My friend, it's been nice to get to know you
| But it's time that I said "so long"

| *(spoken)*
| *(BIG JIM)* Jax, what are you saying? Big Jim,  he's making you an offer. Gold, good money. You'd be a fool to turn it down, my friend. Can't I count on you to take care of our little pest problem in the mill?
| *(JAX)* Jim, I got a secret for ya.
| *(BIG JIM)* Yeah?
| *(JAX)* Yeah, come 'ere. *(JAX leans in real close)*

| Always go while the goin' is good
| Don't wait to even say "Adieu!"
| A beast to bump, do I look like a chump?
| I ain't doin' no fetch quest for you!
| That a roguish gent wears a welcome out quickly
| Is a fact that is understood
| I got my plan to get out while I can
| I'll go while the goin' is good!

**GM:** *(to BERNADETTE and GWEN)* What about you two? You're going to the Red Mill, right?

**GWEN:** Sorry, I don't have time for side quests right now.

**GM:** Side quests!? This is the adventure! You're not curious about the mystery? Not interested in the obvious plot hooks that I planted for you?

**BERNADETTE:** No offense, but you didn't give us much of a mystery or plot hooks - it's just a creepy place with a killer monster, right?

**GM:** Just a - what kind of game do you think this is?


## 8 - You Never Can Tell About Your Players

| You can tell about the weather if it's going to rain or shine
| You can play the odds in poker and you're apt to do just fine
| You may guess at whether Netflix will renew your favorite show
| But to figure out your players is to never, ever know

| The trouble is you can't tell what they want from a campaign
| And what they want tomorrow isn't what they want today
| You give them hard encounters, why you'll only make them mad
| And if you do the opposite you're sure to get in bad

| For you never can tell about your players
| What gonzo sort of stunt they'll try today
| You think that they'll battle the skeleton mage
| But they'd rather make him their valet

| You're never certain that they like your dungeons
| You're often very certain that they don't
| GMs may fancy still that they have the strongest will
| But the players have the strongest "won't"

| I remember just last year when martial classes were in vogue
| I said "listen, if you died would you still roll another rogue?"
| And when he answered "No, I think that I would play a monk,"
| I dropped him in a spike trap, now he's in a sulking funk?

| I told him that his rogue was underpowered and outgunned
| He looked at me confused and said his character was fun
| What's fun about suboptimal? I cannot understand 
| These players think that crawling mega-dungeons is just bland?

| For you never can tell about your players
| Perhaps that's why we often fudge the dice
| You give them instructions in clear, plain terms
| Then they build some Rube Goldberg device

| You're never certain that they like your dungeons
| You're often very certain that they don't
| GMs may fancy still that they have the strongest will
| But the players have the strongest "won't"

**JAX:** Next, I'm going to head down to Bernadette's ship, and -

**GM:** Not so fast! You've been avoiding your bill at the inn, right? So you'll have to sneak out. Make a stealth check.

**JAX:** ...for walking out the front door?

**GM:** Ok, then, just make a luck roll.

**BERNADETTE:** There's no such thing as a luck roll in this game! Is there?

**GM:** I wanted to go over the rules beforehand, remember? There are luck rolls. Now roll.

*(JAX rolls, then groans at the bad roll)*

**GM:** *(clearly pleased)* Oh that's too bad. Just as you go to open the door, it opens from the outside and in steps a very stern-looking woman.

*(points to the SHERIFF, currently an unnamed NPC, who moves to the entrance of the inn and enters. BERNADETTE and GWEN hide. Through the scene, ELANDRA tries to keep SHERIFF distracted so that BERNADETTE and GWEN can sneak out. Several times, they think that they have a clear line, but the SHERIFF turns around suddenly, forcing them to hide again.)*

It's the Sheriff!

**SHERIFF:** *(pointing to JAX)* You -

**NPCs:** *(raising hands in celebration)* Sheriff!

*(SHERIFF glares at the other NPCs)*

**NPCs:** *(whispering to one-another)* sheriff...

**SHERIFF:** You there! I've been looking for you! The time has come to pay your bill. Hand over the gold owed this very instant!

**JAX:** Yeah? Or else what?

**SHERIFF:** Or else I'll take you away to a dark cell, where you will do hard labor until you have worked off your debt.

**JAX:** Ah, see that's no good. I'm just terrible at hard labor. Too hard, really. I think that you'll be better off if I just go and get out of your hair.

*(JAX tries to leave, SHERIFF blocks them)*

**SHERIFF:** You adventuring types! Always thinking you're above the law. Well, in this town everyone obeys the sheriff!

**JAX:** Oh? Maybe we should talk to them, then?

**SHERIFF:** _I'm_ the sheriff you impertinent rogue!

**JAX:** Bard.

**SHERIFF:** Scoundrel!

**JAX:** That's fine too.

**ELANDRA:** Say, Sister, maybe we don't need to be so hard on poor Jax. They're clearly sorry. 

*(ELANDRA gives JAX a meaningful look while BERNADETTE and GWEN try to sneak behind the SHERIFF)*

Aren't you, Jax?

**JAX:** Uh, yeah. I'm very, very, sorry?

**ELANDRA:** And you feel bad about it.

**JAX:** I'm all broken up.

**ELANDRA:** So you're going to work here at the inn until you have paid off your debt.

**JAX:** It is the least that - wait, what? No, I need to leave tonight with...

*(ELANDRA, BERNADETTE, and GWEN all signal JAX to shut up from out of SHERIFF's sight line)*

That is - I _did_ have a pending appointment, but out of the sense of goodwill and decency I feel toward Elandra, I'll reschedule? Yeah, and I'll stay here to work off my debt.

**SHERIFF:** Very well, as long as that works for you, Elandra, I - *(finally spots BERNADETTE and GWEN just as they are nearly out)*

*(CUE MUSIC: "9 - Initiative")*


## 9 - Arrest

*(During the song, NPCs other than ELANDRA and SHERIFF quietly exit, making themselves scarce in anticipation of the coming fight)*

| My Gwendolyn, with Bernadette?
| Betraying me! Ungrateful brat!
| You mean to run out on your groom!
| I suffer and strain, arranging this day.
| To spite me, you'd throw it away?

| These vagabonds you call your friends
| Are turning you so insolent
| What's left of the daughter I raised?

| Don't lie to me about your cruel intent
| *(BERNADETTE)* Lay off her she's free to decide
| little girl!
| Obey me or accept your punishment
| *(BERNADETTE)* You can't force her to be a bride
| in a cell!

| *(GWENDOLYN)* Oh, mother please, I beg for peace
| I always do whatever you ask
| *(SHERIFF)* You show to me disloyalty!
| I've sacrificed! Enough of your sass!

| Your decency forsake?
| You'd throw your life away?
| You won't obey?
| In jail you'll stay
| Away!

| *(JAX)*
| The sheriff crows
| The sheriff blows
| And yet
| My only fear
| Her breathe! Her breathe! The stench!
| *(SHERIFF)* Stay out! What's she to you?
| *(JAX)* Her breathe! I think I'll faint!

| *(ELANDRA)* My sister, hear your daughter's pleading words
| *(GWENDOLYN)* No you'll only make her angry
| *(SHERIFF)* You spinster, your opinion has no worth
| *(GWENDOLYN)* If you must blame someone, blame me

| *(SHERIFF)* You never wed! And now
| *(GWEN)* I cannot watch
| *(SHERIFF)* Look at yourself
| *(ELANDRA)* Do not say it, bite your tongue
| *(SHERIFF)* A lonely maid
| *(ELANDRA)* Words from you have always stung
| *(SHERIFF)* And nothing else

| *(BERNADETTE)* You cannot win! We do not fear.
| *(SHERIFF)* I see it all too clear

| That you! And you! And you!
| Conspire to thwart my will!
| Submit! I'll hang you all!

| *(GWEN)* I must act fast, I make a Calm Emotions cast

*(roll dice, it's a good roll)*


## 10 - Lest My Family

*(ELANDRA's words are all directed at the SHERIFF. The SHERIFF's are directed at GWEN. JAX and BERNADETTE are largely talking to each other. GWEN is talking to the others, but also convincing herself.)*

| *(GWEN)* Lest my family and friends be harmed
| *(BERNADETTE/SHERIFF)* Who, us?
| *(GWEN)* Cease your vi-o-lence and lay down arms
| *(ELANDRA)* At once!
| *(GWEN)* For to quarrel is to double trouble
| *(BERNADETTE/SHERIFF)* For Gwen we may
| *(ELANDRA)* Thank the heavens for this sense
| *(GWEN)* There's been enough of that for now! Oh!
| *(BERNADETTE/SHERIFF)* Belay
| *(ELANDRA)* We can speak and share intents

| *(JAX)* Too bad!
| *(GWEN)* Bernadette I couldn't bear to see
| *(BERNADETTE)* Dear Gwen
| *(GWEN)* You arrested on account of me
| *(BERNADETTE)* Forfend
| *(GWEN)* Or any one of you to die, safe to say that rather I'd
| *(GWEN)* Still be married on the 'morrow
| *(ELANDRA)* We can still talk this out

| *(SHERIFF)* Your plea is moving to my ears, dear Gwen
| *(GWEN)* Forgo
| *(SHERIFF)* I'd be willing to forgive your friends
| *(GWEN)* Flurried blows
| *(SHERIFF)* Glad to hear you're coming to your senses
| *(JAX/BERNADETTE)* We had her, I'm sure
| *(SHERIFF)* I've had enough of your offenses
| *(GWEN)* I've given too many offenses
| *(ELANDRA)* Will you really make Gwen wait in prison?
| *(JAX/BERNADETTE)* in prison now?

| *(GWEN)* Lest my family and friends be harmed
| *(ELANDRA/SHERIFF)* be harmed
| *(JAX/BERNADETTE)* We'll just
| *(GWEN)* Cease your vi-o-lence and lay down arms
| *(ELANDRA/SHERIFF)* vi-o-lence and lay down arms
| *(JAX/BERNADETTE)* Jailbreak;
| *(GWEN)* Don't want any trouble here I regret causing all this fear so please
| *(ELANDRA/SHERIFF)* here you regret causing all this fear so please
| *(JAX/BERNADETTE)* for now
| *(ALL)* Stand down

**BERNADETTE:** But Gwen -

**GWEN:** Just hold, Bernadette. Sometimes fighting only makes things worse. I appreciate that you would have helped me, but it was not to be. *(to SHERIFF)* I'm ready. Arrest me.

**SHERIFF:** *(putting GWEN in handcuffs)* Come now, no need to be so dramatic. *(imitating GM)* "Just to be clear," it's only for the night.

**GWEN:** You're right. What's one more night of imprisonment, compared with the rest of my life?

**SHERIFF:** I hardly know what to do with you, Gwendolyn! But tomorrow, you'll be the baron's problem, and I'll never have any problems again. Mother of a baroness - I'll have my own manor! And servants to do my bidding. Ones who'll actually follow orders, too, not like my willful daughter! A night in a cell will do you some good!

*(exit SHERIFF and GWEN)*

**ELANDRA:** I'm sorry, Bernadette. I know that Gwen is your friend.

**BERNADETTE:** Then help us!

**ELANDRA:** I tried, but we've lost. She'll be locked away now, beyond our reach.

**BERNADETTE:** Hardly! I bet you know right where your sister keeps the keys. If we could only get you in the jail...

**ELANDRA:** I suppose. But she'll be guarding it herself, I'm sure. Something would need to distract her so that I could get in.

**BERNADETTE:** Leave that to us.

**JAX:** Us? I'm in enough trouble.

**BERNADETTE:** But you're the "forceful chauffeur", remember? If passage on my ship was reward enough to kidnap a baron, surely it would be enough to get Gwen out of prison?

**JAX:** That's really not the same thing...

**ELANDRA:** Jax, we need your help. If you can get Gwen safely away on Bernadette's ship, I'll consider your debt repaid.

**JAX:** Well, when you put it that way... A jailbreak is a lot more fun than bussing tables. I'm in. What you need is someone to work up a good, old-fashioned mob.

**BERNADETTE:** Can you do that?

**JAX:** I don't have enough fingers to count the mobs I've assembled. Mostly they come together to run me out of town, but the principal is the same. After sundown?

**ELANDRA:** *(nervously, hiding something)* No, before. Why wait? Let's go at once.

*(exit JAX, BERNADETTE, ELANDRA, discussing plans)*

*(CUE MUSIC: "11 - Act 1 Interlude (Instrumental)")*

*(As the long piano intro begins and the GM speaks, NPCs set the stage for the jail. A barred window for GWEN to be behind, and other trappings.)*

**GM:** We move now to the town jail. Not much of a jail, perhaps, for this small village has seen few prisoners. Maybe an angry drunk who needs to cool off for the night, or the occasional bandit. Still, it's more than enough to hold Gwendolyn, who has been left all alone in this locked cell. The sheriff guards the only way in or out of the building, but stands outside, taking in the warm afternoon air. Gwen, before anyone arrives, tell us what you are doing?


## 12 - You Belong to You

*(GWEN is behind a barred cell window)*

| *(GWEN)*
| There is a peace, now that I can cease
| My struggles, vainly sought
| My sights will set, on a safer bet
| In doing what I ought

| Had I strength before, to stay evermore
| The smiling, obedient child
| And to recognize, in her pitiless eyes
| The long-fated walk down the aisle

| Obligations, family binds
| I do owe my life
| Mother raised me, I defied
| My will the cause of strife
| Headstrong daughters, so they say
| Cause such stress undue
| With the baron, I could stay
| Dutiful, grateful, and true

*(Enter ELANDRA outside of window)*

| *(ELANDRA)*
| Oh Gwendolyn you have always been
| Too generous with your heart
| The loyalty paid to your mother's misplaced
| She isn't your end just your start

| Heartache lingers, near as strong
| As the call of home
| Though your mother's held you long
| Time has come to roam
| Leave behind this fleeting peace
| Heart will guide you true
| Never forget, beloved niece
| You belong to you

## 13 - Release Her!

*(Lights transition to show SHERIFF guarding jail. JAX, BERNADETTE, and the CHORUS [as a mob] approach)*

| *(JAX)* She may fight, beware
| *(BERNADETTE)* Let her if she dare
| *(BOTH)* Stand aside
| Thus defied
| We fear no harm from you
| *(JAX)* Blustering, *(BERNADETTE)* blundering,
| *(JAX)* blubbering, *(BERNADETTE)* blathering
| *(BOTH)* fool!

| *(SHERIFF - spoken)* What are you doing here? You must leave at once!
| *(JAX/BERNADETTE/CHORUS)* Fie!
| *(SHERIFF - spoken)* Gwendolyn is *my* daughter, and will marry whomever I choose!
| *(JAX/BERNADETTE/CHORUS)* Bah!
| *(SHERIFF - spoken)* You dare defy me? Have you no respect for my position as mother and sheriff?
| *(JAX/BERNADETTE/CHORUS)* No!
| *(SHERIFF - spoken)* You would risk my wrath just to make this pointless demonstration?
| *(JAX/BERNADETTE/CHORUS)* Yes!
| *(SHERIFF - spoken)* Begone! Or I shall arrest you as well!

*(SHERIFF leaves guard position to menace everyone. BERNADETTE and JAX dodge gleefully. ELANDRA sneaks in and then out with GWEN during this verse, after which SHERIFF gives up on the mob and retreats inside.)*

| *(JAX/BERNADETTE/CHORUS)*
| Release your daughter now too long you have delayed
| Release your daughter now too long you have delayed
| Release her! Release her! Your selfishness will surely be repaid
| Release her! Release her! Your selfishness will surely be repaid
| Release our friend too long you have delayed
| Your selfishness will surely be repaid
| Consent! Our courage is undaunted
| Relent! This marriage is unwanted
| Release our Gwendolyn she must be free

*(SHERIFF runs out of jail, having discovered that GWEN is missing)*

| *(SHERIFF/JAX/BERNADETTE, and CHORUS from off stage)*
| Gone! Gone! Gone! Gone!

*(JAX and BERNADETTE dance together to the end of the song. They are wary, but are starting to have fun playing together, and are celebrating this victory.)*

*(SHERIFF and CHORUS leave stage, PLAYERS and GM return to OOC)*

**GM:** Alright, you've rescued Gwen from the sheriff, but just to be clear: she's going to be looking for you all and guarding Bernadette's ship. And I doubt that this time you'll be able to talk your way out of it. Where are you planning to hide?

**GWEN:** I was thinking about that. What do you all think if we hide in the Red Mill? It's the one place that nobody in the town - not even my mother - would dare go. We can wait there until after the wedding.

*(GWEN winks to the GM)*

**GM:** Yes, that's a great idea! Oh, I could just kiss you!

*(GWEN leans in, thinking that he means it literally, but the GM turns away from her, too focused on the game)*

Let's get this train back on track and explore the dungeon!

*(JAX and BERNADETTE agree)*

**GM:** Then it's settled. With absolutely no further ado whatsoever, we venture into the Red Mill! Everyone ready your dice, because-

*(Doorbell rings.)*

**PLAYERS:** Pizza's here!

*(Enter THRULSNARD, dressed as half barbarian, half pizza delivery person, carrying a pizza box and scowling. He stomps in, drops the pizza on the table, and starts to silently leave. JAX is barely restraining a laugh.)*

**JAX:** Thanks Greg!

**THRULSNARD:** You guys are jerks.

*(Everyone glares at JAX. JAX grins wickedly.)*

*(Lights, end of act 1)*


<div class="act"></div>

# Act 2

## 14 - Idle Boasting

| *(PLAYERS)* As presented, we've invented
| Many thoughts about what waits inside the dungeon mill.
| *(GM)* Like what?
| *(PLAYERS)* Undead fighters? Giant spiders?
| Or it could be nothing more than lots of rats to kill
| *(GM)* It's not
| *(PLAYERS)* Doesn't matter, on a platter
| Serve a bit of bardic patter and we'll call it done
| *(GM)* You think?
| *(PLAYERS)* After trauma, back to drama
| All in all we'll have a rousing night of fun

| *(GM)* Well I doubt it, think about it
| Though I do agree we're going to have fun

| *(PLAYERS)* Think we're weaklings? *(GM)* Broadly speaking 
| You did run away from yesterday's encounter *(PLAYERS)* Not so!
| With the sheriff? That's not fair; if
| Gwendolyn had let her comrades circle 'round her
| *(GM)* Oh, ho?
| *(PLAYERS)* We'd have trounced her, and pronounced her.
| Booped her snoot and kept her loot and made some good XP
| *(GM)* Of course
| *(PLAYERS)* Like this dungeon we'll be plund'rin'
| And this so-called "beast" we'll easily defeat

| *(GM)* So you guess, unassessed, you can best this mess unstressed?
| *(PLAYERS)* Yes we do! View our thews; bid this beaten beast adieu

| *(Simultaneously)*
| *(GM)* Not the least, for this beast, is a legendary fight
| *(PLAYERS)* It's the least, with this beast, there'll be barely any fight

| *(Simultaneously)*
| *(GM)* It's a legendary fight
| *(PLAYERS)* There'll be barely any fight

| *(GM)*
| What's this, idle boasting?
| You should rather be afraid
| Your characters may meet a grisly fate
| Let me tell you the legend of the mill


## 15 - Legend of the Mill

*(While the GM is singing, CHORUS NPCs stage a little show of his story. When a character is first named (King Johann or Wilhelmina), the GM points to a previously unnamed NPC to name them. As before, all of the other NPCs raise their hands and shout the name in celebration. JAX is on their phone the whole time.)*

### Lyrics

| Old King Johann in days that are gone was ruler of all on high
| With blinding light his magical might could rattle the land and sky
| He Wilhelmina sought for his queen, a woman of great allure
| Wilhelmina refused, King Johann was disabused, and she escaped from the churlish boor
| *(BERNADETTE/GWEN)* and she escaped *(GM)* from the churlish boor 
| *(BERNADETTE/GWEN)* escaped *(GM)* from the churlish boor

| *(Chorus)*
| Johann
| Was a royal sort of Don Juan
| Made a frightful oath that by his powers manful
| He would wed the fairest damsel
| That had ever been born

| She fled for days, 'till she came to this place, still refusing the wedding veil
| In bitter rage he cursed the poor maid, that each night a new form prevails
| Until some soul beholds the foul troll, and finds in his heart love still
| Now at twelve ev'ry night there's a creature of great fright that stalks the tower of the old red mill
| *(BERNADETTE/GWEN)* it stalks the tow'r *(GM)* of the old red mill
| *(BERNADETTE/GWEN)* the tow'r *(GM)* of the old red mill

| *(Chorus abbrev)*
| Johann
| Thought his victory was foregone
| But the frightful curse he'd laid would mark his grave
| She did destroy the wretched mage
| He wished he'd never been born

**GM:** What do you think? I came up with that while we were eating pizza. Trying to work with what you gave me, add a little more excitement and backstory to this boss fight, you know?

**JAX:** *(on their phone)* Wait, what? I wasn't listening.

**GM:** I just said that I came up with that to try to weave in some more backstory for you all.

**JAX:** Yeah that's the part I missed - what were you saying about the mill?

**GM:** Seriously? You missed the whole legend? Ok, I guess I'll start again.

*(CUE MUSIC: "16 - Legend of the Mill (Summarized)")*

**BERNADETTE:** Wait, let me take a stab at it.

| There once was a jerk, and with magic he cursed, some poor woman whose name I forget
| Until released she turns to a beast, every night in the mill beset
| It sounds to me like true love's the key to resolving this dismal wreck
| If you still are confused, I think you could be excused it's basic'ly the plot from Shrek
| *(JAX/GWEN)* it's basic'ly *(BERNADETTE)* the plot from Shrek
| *(JAX/GWEN)* Fiona's *(BERNADETTE)* plot from Shrek

**GM:** It's not Shrek!

**GWEN:** We can't fight Fiona, that's sad!

**BERNADETTE:** Yeah, poor... what's-her-name.

**GM:** It's not Shrek! No! It plays with the dual nature of humanity, showing that within us all is the capacity for true evil. All it takes is a tragic event to push us over the edge, where we are lost forever. It's inspired by classic tales of lycanthropy! 

**JAX:** Yeah! Like Twilight!

**GM:** I'm not getting into this again but you know very well that despite the loud negativity of the public, Twilight is a compelling story of love and self-identity and-

**BERNADETTE:** Ahem - the dungeon?

**JAX:** All right, let's clear this thing out.

**GM:** Well it won't be so easy as that. Especially for a bunch of bards. Let's see how you do against your first peril - a spike trap!

**GWEN:** *(matter-of-fact)* Featherfall.

**GM:** Ok... what about... a surprise goblin attack!

*(NPCs run onstage and act like goblins)*

**BERNADETTE:** Thunderwave?

*(GOBLINS all fall over)*

**GM:** Hmmm... A gelatinous cube blocks your path! *(points to an NPC)*

**NPCs:** *(raising hands in celebration)* Gelatinous Cube!

*(NPC looks at GM like "what? How am I supposed to do that". Nonverbal back and forth before the NPC holds out their arms in a square-ish posture)*

**JAX:** Command.

*(NPC starts to leave, but stops with GM's words)*

**GM:** Aha! Nice try, but Command only works on a creature that can understand your language! A Gelatinous Cube cannot.

**BERNADETTE:** Ooh! I cast Tongues, then, so it can!

*(JAX and BERNADETTE high five)*

**GM:** Let me see that spell. *(consults book)* Argh! Fine, you know what? Rocks fall, everyone dies.

*(NPCs flee stage in terror)*

**ALL PLAYERS:** Cure Wounds!

**GM:** Look, you can't just cheat your way past every encounter in this dungeon.

**JAX:** Of course we can - we're bards!


## 17 - When You're a Bard

| *(Verse)*
| *(JAX)* The life of a bard can be all jocularity
| Forget intelligence, strength, and dexterity
| *(GWEN)* Sing! Rejoice! Rely on your voice
| *(BERNADETTE)* And charismatic smile
| *(JAX)* Remember your wits when you're facing down danger
| *(GWEN)* Then back to the inn where you'll make friends of strangers
| *(BERNADETTE)* Weave spells with your words, *(JAX)* knowing life is absurd
| *(ALL)* So always trust your guile

| *(Chorus)*
| *(JAX)* When you're a bard, when you're a bard
| Adventure's on your mind
| *(GWEN)* Beat a drum, strum a lute, pluck a harp, blow a flute
| Sing your song to escape any bind
| *(BERNADETTE)* They say you're a knave, it's fame you crave
| They'll keep you under guard
| *(ALL)* But until the tale's told, fortune favors the bold
| When you're a bard

| *(Verse)*
| *(JAX)* There's only so long you can stand to sit gabbing,
| Awaiting your fate when you should be out grabbing it
| *(GWEN)* Far you roam, the road your home
| Your fam'ly the friends you're with
| *(BERNADETTE)* Your legend begins when you step out the door
| It's not just your ego, the crowd demands more
| *(GWEN)* Each day a new stanza, *(JAX)* a lucky bonanza
| *(ALL)* Your life becomes a myth

| *(Chorus)*

### Dialog

**GM:** Fine. You made it through the easy parts of the mill, but that was just the prelude. For now you hear it; the bone-chilling roar of the beast! *(roar from offstage)* You aren't sure if the pounding you hear is the sound of its heavy footfalls approaching, the terrific thumping of your anxious hearts, or the hungry growling of the beast's stomach. In any case, before you can react, it appears! And you think to yourselves that this beast looks very familiar...

*(Enter ELANDRA, in beast form)*

**GWEN:** Wait, that's my aunt!

**GM:** Yes! The whole time the evil beast was your aunt, Elandra! She has been lying to you all, and now you will die at her slavering jaws! *(maniacal laugh)*

**GWEN:** But then, we don't need to fight, we can break the curse ourselves. I love Aunty El! She has always been kind to me.

**BERNADETTE:** And she never supported your mother.

**GM:** Wait, what? I don't know if that will work. The spell was supposed to be lifted by, like, a handsome royal who falls for her.

*(BERNADETTE subconsciously looks at JAX)*

**JAX:** Who me, handsome? Alas, she's not really my type. I'm not into furries.

**BERNADETTE:** *(playful banter, not insult)* You're not? I just assumed, seeing as how you're half jackass.

**GM:** Let me check... *(thumbs through module book)* No, it just says "love". I guess family love would count. But you'll need to make a pretty good case to her - it's not an easy roll to break the spell.

**GWEN:** I think that I know just what to say.


## 18 - Love Is

| *(GWEN)*
| Love is a narrow, prescriptive word
| Calling to mind a dance
| Bold gallant prince takes a girl demure
| Blushing just at the chance
| Each step of this tango is preordained
| Beautiful, classical mime
| But in the routine and steps byzantine
| We miss the whole reason why

| Love is not the dance steps
| Not the ballroom pair
| Joyful though romance gets
| It's not our only care
| Look beyond distracting
| Choreography
| Love is the music; melody's movement
| Flowing free!

| *(ELANDRA, GWEN echoing)*
| Love is not the dance steps
| Not the ballroom pair
| Joyful though romance gets
| Friends and family care
| Silly Cupid's shuffle
| Never set me free
| All this time I've had the love
| You give to me

**ELANDRA:** I've waited and waited. So many dashed hopes - I thought that I would be doomed to this fate forever. Night after night, they come to kill me, and I knew that one day they would succeed, but I always hoped in that slim chance. Of love at first sight. For someone to come sweep me off my feet.

**GWEN:** Oh Aunt, I never realized. You must have been so lonely!

**ELANDRA:** Your presence kept me going, Gwen. I thought that maybe, for you, things could be different. You could chart your own course! If I could just get you out from your mother's dreadful influence.

**GWEN:** They are different, thanks to you, and I love you for that. And for everything else that you are. For being you, I love you.

**ELANDRA:** Oh Gwen, I love you too! And I couldn't be more proud of you!

*(GWEN makes a roll)*

**GM:** I can't believe it - that worked. The curse is broken!

*(In unison)*

**GM:** And nobody got to fight anybody!

**ALL PLAYERS:** And nobody had to fight anybody!

*(Players return to OOC, elated, celebrating their victory. Exit ELANDRA)*

**GM:** *(dejected, packing up)* Ok, I guess we're all done. That was a short session, but hey - when you're a bard, right? Skip the cool stuff in the dungeon, avoid the boss - these pages might as well be blank.

**BERNADETTE:** Wait, we don't want to stop yet! We rescued Elandra, but what about the wedding? And the sheriff?

**GM:** You still want to play?

**GWEN:** Yes! We still have to see if we are all able to get away on Bernadette's ship!

**JAX:** I actually have a fun idea for that.

**GM:** But you barely fought anything in the dungeon.

**JAX:** This may surprise you, but fighting monsters is one of the leading causes of death in young adventurers. Nine out of ten clerics agree.

**BERNADETTE:** *(picking up on JAX'S schtick, using a calm medical advertiser's voice)* Talk to your kids about "fighting monsters" today.

**GM:** Hmm...

**GWEN:** Oh hey! Since Aunty El isn't cursed anymore, do you think that she'll come with us on the Arachne? Is that what was holding her back?

**GM:** It was, so I guess she would now. In fact, she'll be thrilled about that! But first you'll have to manage to escape.

*(GM gathers his notes. PLAYERS chatter excitedly)*

Ok, to recap: let's say that the dungeon took you most of the night and it's early morning. Gwen's wedding is later today. Bernadette's ship is sure to be under heavy guard. What do you want to do?

**GWEN:** The ship may be guarded, but my auny and I need to get back to the inn to gather up the things that I packed. Can we slip in there, you think?

**GM:** *(slowly, thinking it over)* Maybe, but the sheriff's thought of that and is guarding the inn herself. With Elandra missing, I think that she put two and two together about who broke you out. *(to JAX)* What's your plan?

**JAX:** Bernadette and I distract the sheriff by posing as detectives and try to "help" her find Gwen. *(to BERNADETTE)* I mean, if you think that would be fun.

**BERNADETTE:** Yeah, I do think that would be fun.

**GM:** Don't you think she'll recognize you?

**BERNADETTE:** Not if we use a little spell called Disguise Self!

**JAX:** Exactly! *(gestures at self and BERNADETTE)* Allow me to introduce the world famous detective duo: the one and only Sherlock Holmes and their assistant...

**BERNADETTE:** Herlock Sholmes!

**GM:** I thought Sherlock's assistant was named John Watson?

**BERNADETTE:** John's not a girl's name. And I'm nobody's assistant!

**GWEN:** You two strike me as more of a "Hardy Boys" pair anyway-

**JAX:** No, no, come on - let's try this again. Ahem! Introducing Sherlock Holmes and their  _partner_...

**BERNADETTE:** Doctor Jane Watson

**GM:** You know what? This is just crazy enough to work. Let's see what the sheriff makes of it.

*(CUE MUSIC: Intro to "You Never Can Tell")*

*(Enter SHERIFF. JAX and BERNADETTE take stage with SHERIFF. Setting is the inn.)*

You see the inn, now empty of patrons, probably due to the unpleasant woman currently haunting the common room. You've made your introductions and offered your services, when she says...

**SHERIFF:** ...and you two think you can help, then?

**JAX:** Madame, you are looking at two of the finest deductioneers in the realm!

**BERNADETTE:** Finest in the world!

**JAX:** And the underworld, too!

**BERNADETTE:** And the sky world... *(to the GM)* is there a sky world in this game?

**GM:** *(with pride)* There is, I call it "Cloudtopolis."

**BERNADETTE:** Really? Nevermind then, that's silly.

**JAX:** *(derisive snort of laughter)*

**SHERIFF:** Deductioneers?

**BERNADETTE:** Try to keep up, Sheriff, we haven't got all morning - the wedding will surely start soon, yes?

**SHERIFF:** Yes! And I need-

**JAX:** *(pacing, pretending to concentrate)* to find your daughter of course-

**BERNADETTE:** *(as though fitting the pieces together)* -the missing bride-

**JAX:** -who is supposed to marry the baron-

**BERNADETTE:** -but who ran off with...

**JAX:** a friend?

**BERNADETTE:** and a mysterious stranger! What a case, Holmes!

**SHERIFF:** That's all exactly right! Incredible! How did you know all that?

**BERNADETTE:** Deduction!

**JAX:** Logic!

**BERNADETTE:** Pure reason!

**SHERIFF:** All of that, though?

**JAX:** Doctor Watson, I think that she wants a demonstration, don't you agree?

**BERNADETTE:** Indubitably, Holmes!


## 19 - Elementary!

| *(JAX)* I deduce that her daughter has disappeared
| *(BERNADETTE)* I deduce that is a keen insight
| *(JAX)* I deduce that she is offering a big reward
| *(BERNADETTE)* I deduce that you are right

| *(JAX)* With logic *(BERNADETTE)* and grace
| *(JAX)* We'll observe every trace
| *(BOTH)* so dear Sheriff there's no need to cry
| *(BERNADETTE)* don't you worry for a minute that we won't connect the dots
| *(BOTH)* And solve the case of the missing bride

| *(BOTH)* Elementary! *(JAX)* Sherlock Holmes on the case
| *(BERNADETTE)* Dr. Watson at their side
| *(BOTH)* Elementary! *(JAX)* Wipe that frown from your face,
| and behold my superior mind!
| *(BERNADETTE)* This pair of gumshoes
| *(JAX)* Will gather the clues
| *(BERNADETTE)* Conduct interviews
| *(BOTH)* And deduce, deduce, deduce!
| Elementary! Yes, the game is afoot
| In the case of the missing bride!

*(BERNADETTE and JAX dance to the dance at the end of the song, happily forgetting that they ever didn't get along)*

**SHERIFF:** Very well, I have just one question for you, then-

**BERNADETTE:** I'll be asking the questions, here. When was Gwendolyn last seen?

**SHERIFF:** Last I saw her she was safely in the jail cell at the station.

**BERNADETTE:** In the jail cell? What crime had she committed?

**SHERIFF:** She was planning to run away before her wedding?

**BERNADETTE:** Eloping, you mean?

**SHERIFF:** Escaping, more like.

**JAX:** Aha! I have made an important deduction, Watson, write this down! The bride did not wish to be wed! She was being coerced!

**BERNADETTE:** But why, Holmes? Why wouldn't she wish to wed the baron?

**SHERIFF:** Because she is a willful child!

**JAX:** An intriguing deduction - for an amateur. If she is so willful then it must have been quite the ordeal to get her in the cell in the first place.

**SHERIFF:** Not at all, she went of her own accord.

**BERNADETTE:** She went willingly?

**SHERIFF:** Well-

**BERNADETTE:** And I remind you that you are under oath!

**SHERIFF:** I am?

**JAX:** Please answer the question.

**SHERIFF:** Wait, am I under investigation?

**JAX:** Doctor Watson, advise your witness to answer the questions asked or I will find her in contempt of court!

**BERNADETTE:** Yes, your honor. I ask you again, Sheriff - did she go to jail willingly?

**SHERIFF:** Not exactly.

**BERNADETTE:** Permission to treat the witness as hostile?

**JAX:** Granted.

**BERNADETTE:** You threw her in the cell by force, didn't you?!

**SHERIFF:** No! I only threatened her friends. And only a little.

**JAX:** *(channeling Judge Judy)* Oh, you only threatened her friends...

**BERNADETTE:** And these friends - they broke her out?

**SHERIFF:** Those gormless fools? I might have thought so, but it happens that I was with them when she escaped. I'm sure that they had something to do with it, though - you should be questioning them!

**JAX:** I have made a second deduction, Doctor Watson!

**BERNADETTE:** And that is?

**JAX:** I deduce that Gwendolyn has very good friends.

**BERNADETTE:** Clearly the best, Holmes.

**SHERIFF:** I object to that.

**JAX:** Overruled.

**BERNADETTE:** *(impersonating Columbo)* And one more thing - this baron that you arranged for your daughter to marry...

**SHERIFF:** Yes?

**BERNADETTE:** He's a good man? Nice fellow? The sort of guy that a woman would like to wed? Not at all some kind of monstrous, overbearing ogre?

**SHERIFF:** I don't see what that has to do with it. He's a baron - that I was able to arrange this wedding at all is a blessing on our house. Gwendolyn should be happy to be able to do something for her future family.

**BERNADETTE:** And her current mother?

**SHERIFF:** So it benefits me, too, what of it?

**BERNADETTE:** *(turning to the audience)* Esteemed members of the jury-

**JAX:** Hang on! You seem confident that the lot of them didn't just leave town. What makes you think that they can even be found?

**SHERIFF:** You know that ship down in the harbor? The Arachne?

**BERNADETTE:** I have a passing familiarity, yes.

**SHERIFF:** That ship belongs to Bernadette, one of her friends, and believe me when I say that she would never leave it behind. *(conspiratorially)* But you know, I had my people search it?

**BERNADETTE:** You what?

**SHERIFF:** Any ship in my harbor is subject to search. And wouldn't you know it, I found contraband.

**BERNADETTE:** *(losing control of the charade)* You don't say...

**SHERIFF:** I do! And what's more, my man at the docks says that the ship is not seaworthy. It's a real shame, but I bear the terrible responsibility of sinking the Arachne, lest some poor souls drown on it on the next voyage. Safety first, you know.

**BERNADETTE:** You wouldn't dare!

**SHERIFF:** What's it to you? I'll do as I see fit in my town.

**JAX:** What she means is - you wouldn't dare sink our best chance of finding your daughter, would you?

**SHERIFF:** How's that?

**JAX:** *(to GM)* I want to cast Suggestion on the sheriff.

**GM:** Ok, let's hear it.

**JAX:** *(to SHERIFF)* Think, Sheriff! With no ship, there'll be nothing holding her here anymore. She, Bernadette, and Jax will be gone in an instant. That ship is just the bait you need.

*(GM rolls, then nods)*

**SHERIFF:** You know, that's a good point. But, I don't think that I mentioned Jax's name...

**JAX:** Deduction.

*(GM rolls again, then shakes his head negatively)*

**GM:** Nope, that slipup is going to cost you.

**SHERIFF:** Of course. And since you mention bait, I have a better use for it. As the new backdrop to the wedding.

**BERNADETTE:** It is a pretty ship.

**SHERIFF:** That rotting schooner? Hardly! But if I move the wedding down to the docks, there's no chance of them sneaking past all of the guests. Everyone in this town will be looking for the bride, right in the very place she's most likely to go. It's perfect. I'll just sink the ship _after_ the ceremony.

**BERNADETTE:** But that's - I mean, no, that will never work, because... because...

**JAX:** There isn't enough time?

**SHERIFF:** You're right! If I don't hurry they'll have already sunk it! I have to get down there right away and start the arrangements. You two guard the inn in case she comes back here!

*(SHERIFF heads to exit)*

**JAX and BERNADETTE:** *(to themselves, OOC)* Yes!

*(SHERIFF looks out the door and stops)*

**SHERIFF:** What? No!

**JAX:** No?

**SHERIFF:** Yes, "no". That untimely baron is pulling up now in his carriage. He's going to find out that Gwendolyn is missing, and this whole wedding will unravel.

**BERNADETTE:** He's coming here? That's no good, we need the inn to be empty...

*(SHERIFF looks suspiciously at BERNADETTE)*

...so that we can search for clues. For the investigation!

**SHERIFF:** Well that's your problem. If you're half as good as you claim to be, I'm sure that you'll manage. But under no circumstances can he learn that Gwendolyn is missing! Just play along with whatever I say, or so help me I'll have your heads!

**GM:** Before I introduce the baron - what kind of person do you think he should be? Should he be a dejected young prince, the sullen black sheep married off in hopes it'll cheer him up? Or a goofy only child with a dashing smile and an inflated ego? Or something else? What would be fun?

**JAX:** The last one! I really want to see you do someone silly for a change.

**BERNADETTE:** Definitely! Oh - make him a serial heartbreaker, a real hopeless romantic who falls in love as easily as the weather changes.

*(CUE MUSIC: "20 - Every Day Is Ladies' Day")*

**GM:** Perfect! Ok I have just the guy in mind, here we go.

*(Enter CHORUS as some of the baron's servants - very prim and proper. They lay out a carpet, wipe down the furniture, clear a space, or otherwise prepare the room for his arrival)*

**SERVANT:** All kneel for his lordship, who graces you with his presence, uh, presently.

*(Enter BARON)*

**NPCs:** *(raising hands in celebration)* Baron!

*(BARON bows and poses, soaking up the attention, calling for applause, but cuts it off before he begins singing)*


## 20 - Every Day is Ladies' Day With Me

| I should like, with out undue reiteration of the ego, to
| Express how very hard I find it is to make my pay go round
| Among my vulgar creditors! I'm fearfully in debt
| For I always have afforded anything that I could get!

| But I must say I've enjoyed the best of what there is in life;
| I've been lucky in my love affairs, I've never had a wife!
| I can summon little int'rest in the dry affairs of state.
| And the businessmen who call on me are coldly left to wait!

| For every day is ladies' day with me
| *(CHORUS)* Every day is ladies' day with him
| *(BARON)* I'm quite at their disposal all the while
| *(CHORUS)* He is at their disposal all the while
| *(BARON)* And my pleasure it is double if they come to me in trouble
| For I always find a way to make them smile
| The little darlings
| I've no doubt I should have married long ago
| *(CHORUS)* Doubtless he should have married long ago
| *(BARON)* It's the proper thing to do you'll all agree
| *(CHORUS)* It is the proper thing we all agree
| *(BARON)* But I never could find any fun in wasting all my time on one
| So every day is ladies' day with me

| *(CHORUS)* Every day is ladies' day with him
| He is at their disposal all the time
| *(BARON)* But I never could find any fun in wasting all my time on *(BARON and CHORUS)* one
| So every day is ladies' day with me/him

| It's a frightful thing to think of all the hearts that I have broken
| Although each one fell in love with me without the slightest token
| That my fatal gift of beauty had inflamed her little heart
| But I found that some small favor always seemed to ease the smart!

| A position for a cousin or a loan to dear papa
| Just a dainty diamond necklace or a meal of fine foie gras
| But I don't begrudge the collarets and necklaces of pearls
| All the money that I ever saved is what I spent on girls

| For every day is ladies' day with me
| *(CHORUS)* Every day is ladies' day with him
| *(BARON)* I'm quite at their disposal all the while
| *(CHORUS)* He is at their disposal all the while
| *(BARON)* And my pleasure it is double if they come to me in trouble
| For I always find a way to make them smile
| The little darlings
| I've no doubt I should have married long ago
| *(CHORUS)* Doubtless he should have married long ago
| *(BARON)* It's the proper thing to do you'll all agree
| *(CHORUS)* It is the proper thing we all agree
| *(BARON)* But I never could find any fun in wasting all my time on one
| So every day is ladies' day with me

| *(CHORUS)* Every day is ladies' day with him
| He is at their disposal all the time
| *(BARON)* But I never could find any fun in wasting all my time on *(BARON and CHORUS)* one
| So every day is ladies' day with me/him

**SHERIFF:** *(kneeling)* Esteemed baron, welcome to Red Mill. I hope that you found the journey comfortable?

**BARON:** Hardly! Man wasn't meant to travel by carriage. Rise, I won't have my future mother-in-law bowing and scraping like a common peasant. We're practically family.

**SHERIFF:** Very soon, your grace, yes.

**BARON:** I must say, I'm eager to meet my bride. Is she as beautiful as I have heard?

**SHERIFF:** Ah, yes, well - it's bad luck to see the bride before the wedding you know, but rest assured, we'll find her- er, that is, *you'll* find her... as lovely as... a sunrise.

**BARON:** A sunrise? Doesn't really make a man's heart pound, does it?

**SHERIFF:** *(nervously)* Correct as always, sir. But nevermind that, please let me introduce Doctor Watson, who is a sort of detective looking to solve a local crime - nothing major, I assure you, this is a safe and orderly town as you know - and would like to speak with you about anything you saw on your journey in. *(To BERNADETTE)* Isn't that right, Doctor Watson?

**BARON:** *(smitten)* Doctor Watson! Lovelier than a sunrise and sunset put together. Throw in the moon and stars on a cloudless night. That is, if you come out at night?

**GM:** Wait wait, that's terrible flirting. I can do better. *(pausing to think)*

*(pause continues, as the GM considers possibilities and then shakes his head. He is not very practiced at flirting, either in the game or out.)*

**JAX:** *(sincerely complimenting BERNADETTE's player)* Come on, just look at her. Say something about her eyes. She has stunning eyes.

**BARON:** Doctor Watson! Are you a beholder? Because I see beauty in your eyes.

*(ALL PLAYERS groan)*

**BERNADETTE:** I liked Jax's version better.

**GM:** No, no, I'll get it, just give me a second.

*(GM thinks, then shrugs)*

**BARON:** Doctor Watson! You're... very... good-looking. Did I mention that I own a castle? Because I do.

*(Everyone looks to the GM)*

**GM:** Come on, let's just go with it.

**BERNADETTE:** *(swooning)* Oh Baron!

*(SHERIFF looks back and forth between the two, furrowing her brow in suspicion. As she opens her mouth to redirect, JAX swoops in to intercept)*

**JAX:** We'd best leave them to it, we've got a lot of work to do, Sheriff. My lord. *(bowing)*

*(JAX nudges the SHERIFF offstage, not pushing but invading her personal space so that she begrudgingly retreats. When SHERIFF leaves, BERNADETTE's demeanor changes - no longer the confident captain, she takes on the mask of a naive and shy young woman, batting her eyelashes at the BARON. SERVANTS wait on the BARON, preparing cushions for chairs, bringing out cups and wine, doting on him.)*

**BARON:** Well now, it is nice to be alone. Not even a chaperone for you, what a modern and intriguing woman!

**BERNADETTE:** Alone? But your servants?

**BARON:** *(looking around, noticing them for the first time)* Are they still here? I hardly notice them, the layabouts. Begone, all of you. You're bothering the good doctor.

*(Exit SERVANTS, bowing and backing out of the room)*

Now we are truly alone. Please, ask me your questions, let me bare my soul to you. I want to hear more about this crime. Or perhaps I just want to hear more of your beautiful voice.

**BERNADETTE:** You flatter me, your grace. But I must confess, I come to you under false pretenses.

**BARON:** Oh?

**BERNADETTE:** I am searching for someone.

**BARON:** I do know a great many people, what is this person's name? Speak it, and I shall command their presence.

**BERNADETTE:** Your grace, I do not know his name yet. I should think that - well, I would know him when I saw him.

**BARON:** Perhaps you can describe him, then? This man you're looking for?

*(CUE MUSIC: "21 - If You Love But Me")*

**BERNADETTE:** I will try... *(to GM)* I want to cast Charm Person.

**GM:** Sure thing. Let's hear it, and I'll make the roll.


## 21 - If You Love But Me

*(During this song, the GM makes the roll needed for Charm Person at a key point, then whispers to the baron just before the Baron comes in.)*

| *(BERNADETTE)*
| Although I'm but a girl of *(pause)* seventeen
| I would so like to be
| Someone's affinity
| I'd like to have him woo me for his queen
| Oh heav'n above
| How I could love

| If he would take me in his arms and call me all his own
| Imagine how delightful that would be
| Forsaking ev'ry other I would cling to him alone
| If he would only say he loved but me

| If he'd say that he loved but me
| Loved but me, only me
| What a paradise life would be
| Life would be!
| When he asked me to name the day
| Name the day, name the day
| I would say right away don't delay
| If you love but me

| *(BARON)* I would say that I love but thee
| *(BERNADETTE)* Love but me? Only me?
| What a paradise life would be
| *(BOTH)* Life would be!
| *(BARON)* So, I ask you to name the day
| Name the day, name the day
| *(BERNADETTE)* Then I say right away don't delay
| If you *(BOTH)* love but me/thee

**BARON:** What joy! What love! What... convenience!

**BERNADETTE:** Convenience?

**BARON:** You say right away, and I, anticipating your every whim, have already arranged a wedding today! It is a simple matter to exchange the brides. I can't imagine the guests will mind.

**BERNADETTE:** *(under breath)* The brides might.

**BARON:** What's that dear?

**BERNADETTE:** I was just swooning, darling. But now, promise me that you mean it.

**BARON:** I do! I profess my love - I would shout it from my castle ramparts!

**BERNADETTE:** But my love, it's bad luck to plan a wedding with two brides. When can you break the news to the other woman?

**BARON:** I will just as soon as I see her. I'll seek her out at once!

**BERNADETTE:** Oh lovely dearest, I just knew that I could leave everything in your strong, capable hands. But say, how do I know that you won't pull something like this on me?

**BARON:** On you? Never! Here, this necklace was to be my wedding gift - and so it is, but to my new bride. Please take it and hold on to it until we see each other again.

**BERNADETTE:** Oh Baron, it's so beautiful! I believe you, I do. If only our wedding could be even sooner!

**BARON:** But, it's going to start at any minute. See at the docks, the guests are already arriving!

**BERNADETTE:** Even a moment is too long, but I will bear it! Until we are wed, sweet Baron.

**BARON:** Soon my beloved... Doctor Watson.

*(Exit BARON. Enter GWEN and ELANDRA, carrying luggage)*

**GWEN:** I thought they'd never leave.

**BERNADETTE:** *(joking)* I thought *you'd* never leave.

*(GWEN doesn't get the joke and looks sad and confused)*

I mean that I assumed you'd stay here for your whole life. Your mother's keelhauled you more times than I can count, but you've always gone back for more.

*(GWEN starts crying)*

No! No, I just mean that I'm proud of you! And excited for us! What's going on?

**GWEN:** You're right! This isn't me! I can't go!

**ELANDRA:** Don't listen to Bernadette, she doesn't know you like I do. You're strong! You can do this!

**GWEN:** But if we leave, poor Mother will have no one. She doesn't have any friends - who will love her when we are gone?

**BERNADETTE:** Um, nobody?

*(GWEN collapses into a chair, sobbing into her hands. ELANDRA gives BERNADETTE a glare and motions for her to help. BERNADETTE shakes her head, but ELANDRA motions more forcefully. BERNADETTE looks for a way out as GWEN cries, then resigns herself to sitting down next to GWEN. Slowly, she lifts her arm and sets it awkwardly around GWEN's shoulders. GWEN turns and grasps her in a tight, moist embrace.)*

Look, Gwen. You care, I get it. That's one of the things I love about you, taking care of everyone. But I'm going to give you a hard truth about your Mom - she doesn't love you.

*(Holding up a hand to quiet GWEN before she can contradict)*

Maybe there was a chance that you two could have worked this out. Maybe. But that was before she locked you up for daring to have an opinion about who you marry! Someone who loves you couldn't do that.

Let your guilt go. You tried. Nothing's forever - maybe with time she'll soften a bit and you can make up. But you can't stay, Gwen. Don't sacrifice yourself for her.

*(JAX bursts in)*

**JAX:** Oh good, you're already sad. I have bad news and I'd hate to be the cause of your sour mood. I couldn't get the sheriff to move the wedding,...

*(BERNADETTE and ELANDRA start to interrupt but Jax forstalls them)*

...and *(to BERNADETTE)* she still wants to sink your boat.

**BERNADETTE:** Ship!

**JAX:** No need for language! The tongue on these sailors.

**BERNADETTE:** I said "ship"!

**JAX:** Then you missed the important part - she's gonna sink it! Burn it down!

**BERNADETTE:** Burn down the Arachne?! I'll burn down her face!

**JAX:** Woah! Don't you know that "lighting something on fire is a classic rookie mistake in improv?"

**BERNADETTE:** This isn't improv, this is war!

**JAX:** Now we agree! Except, I can't see any way for us to get past that wedding. I'm out of spell slots.

**BERNADETTE:** Me too. Gwen?

**GWEN:** I still have one... *(trails off, lost in thought)*

**JAX:** It doesn't matter, there's no one spell that will get us all past anyway. What we need is a plan. Something clever, but needlessly elaborate.

*(GM and PLAYERS look quizzically at JAX)*

What?

**BERNADETTE:** I do have _some_ good news - I managed to get the baron to fall for me. He plans to cancel your wedding. Your mom won't be able to do anything about that, if he's the one who calls it off.

**ELANDRA:** So then... Gwen could stay here. If she wants. The wedding isn't going to happen. Isn't that just what you wanted,  Gwen?

**JAX:** Hey, you're right! We did it!

*(GWEN still looks miserable)*

Uh, hooray?

**GWEN:** I guess so. All I have to do is show my face, and the baron will leave me at the altar, in front of the whole town. And then things will go back to the way they were.

*(GWEN looks around the inn, slowly deflating)*

My mother will be furious, but she'll calm down. *(trying to convince herself)* I don't have to leave. I - I could stay... I do have responsibilities here.

*(pause)*

But what would you do, Bernadette?

**BERNADETTE:** You know me; sitting still isn't in my nature.

**GWEN:** So I should go?

**BERNADETTE:** No, _I_ should go. But, Gwen, don't you want to come with me?

**GWEN:** I... It's not about what I want. It's about what I have to do. I can't just "choose". Choosing not to marry is what started all of this trouble.

**JAX:** Lady, my experience is that no matter what you do or where you go, trouble's already there. There's no avoiding it, so just chase after whatever strikes your fancy.

**GWEN:** The motto of criminals and cowards!

**JAX:** Ha! She's trained you well. As soon as you start thinking about your own desires, your mother's words jump out of your mouth to drown it out!

**GWEN:** It's not that simple, I have to -

**JAX:** "Have to", "have to", "have to" - you wish! You're the only person I ever met that heard the term "free will" and took it to mean that your will is worthless! This isn't supposed to be hard, you know. Just tell us what you want!

**GWEN:** What I want? Bernadette, of course I want to sail away with you! I dream about it every night. When I hear a story of some far off place, some magical wonder - I picture you there, and try to imagine how I would feel if only... When you leave I get so jealous that my stomach turns dark and sour, and when you return your tales are a brief taste of sweet honey that only leaves me hungrier. "Want" doesn't begin to describe it.

**BERNADETTE:** You have no idea how happy I am to hear that. What's stopping you?

**GWEN:** I guess... I'm not sure.

*(CUE MUSIC: "22 - Adventure of Our Dreams (Reprise)")*

| *(GWEN - Recitative)*
| I don't have to go!
| And I don't have to stay!
| I can choose - and I choose to leave!

| *(BOTH)*
| An adventure beyond any other
| By the breeze and the seas we'll be borne
| On a whim we'll decide, simply flow with the the tides
| Or face down the rage of the storm

| When we tire we will lean on each other
| And soon laughter will flow like a stream
| As we sail o'er the sea
| *(GWEN)* me and you *(reaching out)*
| *(BERNADETTE, taking GWEN's hands)* you and *(BOTH)* me,
| *(JAX, adding their hand on top, like a sports team huddle)* and me,
| *(ELANDRA, following JAX)* and me,
| *(ALL)* The adventure of our dreams

**BERNADETTE:** You won't regret this, Gwen. We're going to see the world together!

**JAX:** Sure, beautiful, but _how_ are we supposed to get there from here?

*(CUE MUSIC: "23 - Ding Dong Wedding Bells")*

**GWEN:** It's starting! *(giddy, nearly manic)* Don't worry, I have a plan. Just take me down there, act natural, and follow my lead. Aunty El?

**ELANDRA:** I'm coming with you, Gwen. I always will.

*(GWEN leads ELANDRA, JAX, and BERNADETTE out of the inn and offstage. CHORUS sets the stage for the wedding while singing "Ding Dong Wedding Bells". GM directs.)*

| Welcome this day of gladness!
| Banish all thought of sorrow and sadness!
| Let ev'ry heart be singing!
| Glad wedding bells be ringing out their joy!

| Ding Dong Ding Ding
| Sending their silver-voiced
| message far and wide.
| Ding Dong Ding Ding
| Long life attend our
| Baron and his bride.

| Ding dong wedding bells
| Ding dong wedding bells
| Ding dong bells

*(CHORUS takes seats as wedding guests. Enter SHERIFF, taking center stage and looking out toward the audience. She waits impatiently, maybe tapping her foot or fidgeting with her shackles. Enter JAX (as Holmes), BERNADETTE (as Watson), holding GWEN as a captive. They enter behind the SHERIFF, unseen and unheard by her.)*

**GM:** The wedding setup is truly breathtaking, and the gentle breeze from the ocean mixed in with the calm light of evening brings a sense of serenity. But at least one person at the wedding is anything but serene. The sheriff is waiting impatiently, perhaps even nervously, and you hear her say to herself...

**SHERIFF:** *(to herself)* Where are those detectives? 

**JAX:** Deductioneers.

**SHERIFF:** Yes, that's what I mean- hold it! You found her?

**BERNADETTE:** It was, as they say, -

**SHERIFF:** Elementary?

**JAX:** My dear Watson! Have we ever been so insulted?

**BERNADETTE:** Never, Holmes! Sheriff, if you think it was so easy, you should have done it yourself!

**SHERIFF:** Fuff! I didn't mean anything by it. It's only that you got here at the last possible second - the wedding's practically started! I was worried stiff that you'd never make it back.

**JAX:** It sounds like your only problem was a lack of faith in us.

**BERNADETTE:** And you can hardly blame us for that!

**SHERIFF:** *(handing over money pouch)* All right, all right. You've earned this reward.

*(CUE MUSIC: "24 - Entrance of the Wedding Guests")*

Now take your seats, I'm eager to have this over with.

**GWEN:** You're telling me!

*(JAX, BERNADETTE, ELANDRA take seats for the wedding. GWEN, SHERIFF go to altar)*


## 24 - Entrance of the Wedding Guests

| *(BERNADETTE, CHORUS, ELANDRA, GM, JAX)*
| We come, every guest, in their best!
| Fitly dressed for the nuptial merry-making
| And we wait with pride
| To greet the beautiful bride
| Whom today so noble a consort is taking

| So sing joyous loud and be proud
| Not a cloud mars the bliss of the betrothal
| May no sorrow or strife
| By any chance enter their life
| The baron and his bride!

| *(SHERIFF)*
| My friends I thank you for this first-
| *(cut off by entrance of BARON)*

*(Enter BARON)*

**BARON:** Wait! Stop! Stop the wedding!

**ALL:** *(gasp and murmur)*

*(ACCOMPANIST continues)*

**BARON:** That includes you, accompanist!

*(ACCOMPANIST stops)*

**SHERIFF:** My liege, what has happened?

**BARON:** My friends, you have come for a wedding, and *(imitating GM)* "just to be clear", let me assure you that - today - a wedding you shall have.

**SHERIFF:** That's a relief, I've had about as much-

**BARON:** Yes, we shall all sing and dance today! For I have, at last, found the one with whom I can settle down. Perhaps raise some little barons or baronettes of my own.

**SHERIFF:** Wonderful, let us continue with the ceremon-

**BARON:** We shall all sing and dance today - save one. *(Down on one knee)* Dearest, er...?

**GWEN:** Gwendolyn.

**BARON:** Yes, dearest Gwendolyn. Though it pains me to say it, we royals are burdened to follow the call of our noble hearts, wherever they may lead. And so today, on the very brink of our wedding, I must tell you, that I have fallen in love - with another!

**ALL:** *(feign surprise)*

**SHERIFF:** But, my lord - the planning - the wedding - my manor?!

*(JAX, BERNADETTE, and ELANDRA begin sneaking onto BERNADETTE's ship during the next interactions)*

**GWEN:** Oh! What sorrow that the first words I should ever hear from my fiance's lips are these!

**BARON:** I am sorry-

**GWEN:** Agony! Heartbreak! Tears! Is there nothing that could ease this pain?

**BARON:** Please, take this. The ring I was to give you, with the finest emeralds in the realm. Keep it and remember me, and may this token ease your grief.

**GWEN:** Well... ok!

*(GWEN scampers onto the ship. CHORUS of guests begin muttering restlessly)*

**BARON:** But friends, fear not, for a wedding you were promised, and a wedding you still shall have! Allow me to introduce the bride - the lovely, incomparable, Doctor Watson!

**BERNADETTE:** *(on ship, removing disguise)* Actually, I prefer "Captain".

**SHERIFF:** Bernadette! *You* are going to marry the baron?!

**BERNADETTE:** Well he seems like such a nice baron, but I'm already married to the sea! Besides, we've got a schedule to keep, and we're casting off - oh - a few moments ago! Goodbye!

**GWEN and JAX:** And thanks for the wedding gifts!

*(JAX, BERNADETTE, GWEN, and ELANDRA wave from the ship, brandishing the various things that they stole)*

*(CUE MUSIC: "26 - When You're a Bard (Reprise)", but cut off when SHERIFF boards)*

**SHERIFF:** *(jumping aboard, taking ELANDRA hostage)* Hold it right there! Stop this ship at once, or I'll -

**GWEN:** You'll what? Threaten us some more? Murder your own sister in front of this crowd?

**SHERIFF:** What choice do I have, Gwendolyn? Everything that I've worked for, you've just tossed away! How could you do this to me? Your own mother?

**GWEN:** *To* you? This isn't about you! All I ever wanted was to stay here with you and Aunty El, and live my life. But if you won't let me have that, then I'll go somewhere else, and be with people who love me and who will look out for me. That's all I ever wanted.

**SHERIFF:** *(releasing ELANDRA)* Enough of this foolishness. You will leave this ship at once, Gwendolyn, and return home! I command it!

*(CUE MUSIC: "25 - Command". Lines are spoken over chord changes.)*

**GWEN:** No! You can't command me anymore.

*(MUSIC: chord change)*

**GWEN:** *(to GM)* I cast Command.

*(MUSIC: chord change)*

**GM:** You get one word for your command.

*(MUSIC: chord change)*

**GWEN:** *(to SHERIFF)* Leave!

*(MUSIC: continues building during actions)*

*(GM rolls dice. Wordlessly, SHERIFF goes glassy-eyed and exits)*

*(MUSIC: holds a final chord)*

**JAX:** *(breaking tension, in time with music)* Gone!

**ALL (joining in):** Gone! Gone! Gone!

*(Short dance, exit all NPCs. End MUSIC. PLAYERS and GM return to seats, cheering and celebrating)*

**GWEN:** That was perfect!

**JAX:** Absolutely awesome!

**GM:** That was easily the best session we've ever had. Wow!

*(awkward pause)*

**JAX:** So... we're going to turn this into a campaign, right?

**GWEN:** Can we?

**BERNADETTE:** I only agreed to come to this one game, but *(smiling at JAX)* I am having a great time.

**JAX:** Me too!

**GM:** Really? Yes! Of course! Do you all have any ideas on what we might do next?

**JAX:** Can it be something with treasure?

**GM:** Sure! How about if you find some mysterious treasure maps in Bernadette's locked strongbox?

**BERNADETTE:** Oooh! And what if I don't remember putting them there? So there's some mystery.

**GWEN:** And maybe they are signed by your estranged brother?

**BERNADETTE:** Jean Claude? I've been looking for him since I first left home to become a sailor! We have to follow these maps!

**GM:** I think that'll work! I'll put some stuff together for next week.

**JAX:** *(moving closer to GM to share notes)* Great! I have a few more ideas, too...

*(As before, JAX and GM continue conversation silently, and lights focus on BERNADETTE and GWEN)*

**GWEN:** So... no aneurysm, then?

**BERNADETTE:** Hey, just being a good wing woman. Now's your chance, by the way.

*(BERNADETTE and GWEN look toward GM. Lights direct focus to GM/JAX, and their conversation becomes audible)*

**GM:** ...just to be clear: you're talking about assembling a whole armada? That sounds awesome, I'll dig up my supplements about ship-to-ship combat...

*(Lights return focus to GWEN/BERNADETTE)*

**GWEN:** *(no longer interested at all)* Eh... well... something about his aura... *(shrugs)*

*(Lights up on everyone again)*

**JAX:** *(worried)* Wait! Do you think Thrulsnard made it on the boat?

*(CUE MUSIC: "26 - When You're a Bard". Enter full cast as the song plays.)*

| *(BERNADETTE/JAX)* When you're a bard, when you're a bard
| Adventure's on your mind
| *(GWEN/GM)* Beat a drum, strum a lute, pluck a harp, blow a flute
| Sing your song to escape any bind
| *(SHERIFF/ELANDRA/BARON)* They say you're a knave, it's fame you crave
| They'll keep you under guard
| *(ALL CAST)* But until the tale's told, fortune favors the bold
| When you're a bard

*(Outro, curtain call)*



## Credits

Most music and some lyrics and plot points derived from "The Red Mill", a 1906 operetta with music by Victor Herbert and libretto by Henry Blossom, which is in the public domain. This adaptation was done by Brian Shourd.

Thrulsnard's song is an excerpt from "Night on Bald Mountain" by Modest Mussorgsky and arranged by Konstantin Chernov, which is in the public domain.

This work includes material taken from the System Reference Document 5.1 (“SRD 5.1”) by Wizards of the Coast LLC and available at [https://dnd.wizards.com/resources/systems-reference-document](https://dnd.wizards.com/resources/systems-reference-document). The SRD 5.1 is licensed under the Creative Commons Attribution 4.0 International License available at https://creativecommons.org/licenses/by/4.0/legalcode.

## Acknowledgements

Thank you to everyone at Enoch City Arts for the encouragement, facilities, and inspiration to write this, as well as for producing the show! Without ECA, this would not have been possible. Special thanks to Skyler Shourd, Kevin Bonner, and Bryan Bullock for extensive help during the writing process and never-ending ideas and support. I also want to thank all of the readers who have gone over the various versions of the script and provided invaluable feedback: Ann Bonner, Brendan Freckleton, Caleb Avery Lizon, Colleen Carpenter, J Mark Fillmore, and Janine Chalmers.

## Adaptation Notes

Most of these characters and plot points are heavily modified from the original version. Three of the original songs are kept basically unchanged ("Every Day is Ladies' Day With Me", "If You Love But Me", "Entrance of the Wedding Guests"), several are revised but similar ("The Legend of the Mill", "You Never Can Tell About Your Players", "Go While the Goin' Is Good"), and the rest are basically entirely new lyrics.
