---
title: "'You Never Can Tell' lyrics"
---

These lyrics go along with the audition cut for the song "You Never Can Tell".

## Lyrics

| You can tell about the weather if it's going to rain or shine
| You can play the odds in poker and you're apt to do just fine
| You may guess at whether Netflix will renew your favorite show
| But to figure out your players is to never, ever know

| The trouble is you can't tell what they want from a campaign
| And what they want tomorrow isn't what they want today
| You give them hard encounters, why you'll only make them mad
| And if you do the opposite you're sure to get in bad

| For you never can tell about your players
| What gonzo sort of stunt they'll try today
| You think that they'll battle the skeleton mage
| But they'd rather make him their valet

| You're never certain that they like your dungeons
| You're often very certain that they don't
| GMs may fancy still that they have the strongest will
| But the players have the strongest "won't"
