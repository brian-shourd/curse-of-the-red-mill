---
title: "'Gwendolyn' lyrics"
---

These lyrics go along with the audition cut for the song "Gwendolyn".

## Lyrics

| It seems, in my family, a truth is assumed
| That each woman's born lacking a husband
| So all of my life I've been polished and groomed
| Always knowing I'm bound for a dead end

| My mother the sheriff's tomorrow arranged
| The baron will wed me and I'll be exchanged
| A new family name, but the rest is unchanged
| And each one believes I am theirs

| But I'm planning a life just for me
| I'll not be a wife or lady
| From daughter to bride, never Gwen on my own
| The path that's before me seems written in stone

| Yes tomorrow my wedding arrives
| It's the future my mother contrives
| A sham baroness bound by corset and dress
| But this woman has plans
