---
title: "'When You're a Bard' lyrics"
---

These lyrics go along with the audition cut for the song "When You're a Bard".

## Lyrics

| The life of a bard can be all jocularity
| Forget intelligence, strength, and dexterity
| Sing! Rejoice! Rely on your voice
| And charismatic smile
| Remember your wits when you're facing down danger
| Then back to the inn where you'll make friends of strangers
| Weave spells with your words, knowing life is absurd
| So always trust your guile

| When you're a bard, when you're a bard
| Adventure's on your mind
| Beat a drum, strum a lute, pluck a harp, blow a flute
| Sing your song to escape any bind
| They say you're a knave, it's fame you crave
| They'll keep you under guard
| But until the tale's told, fortune favors the bold
| When you're a bard
