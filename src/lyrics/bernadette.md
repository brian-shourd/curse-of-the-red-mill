---
title: "'Bernadette' lyrics"
---

These lyrics go along with the audition cut for the song "Bernadette".

## Lyrics

| I'll tell you all her history
| For there's an air of mystery
| Regarding Bernadette

| She's known upon the sea
| For her nerve and gallantry
| And her rise to captain from cadet

| From port to port all pirates quail
| The sailors drink and tell the tale
| Of her ferocity and skillful bayonet

| But Gwendolyn, she knows
| That where e'er the captain goes
| She has a lifelong friend in Bernadette

| For Bernadette,
| In each vignette
| Can hold her own, you know she needs no epithet
| She vows to never be a wife
| To sail the sea is her whole life
| She takes the helm and wheel to dance a minuet

| But she has yet
| A sole regret
| That she has left behind a friend she won't forget
| She has a ship at her disposal
| And to Gwen makes this proposal
| To set sail tonight with Bernadette
