---
title: "'Jax' lyrics"
---

These lyrics go along with the audition cut for the song "Jax".

## Lyrics

| Some days you get the worst of luck just wanderin' the town
| *(whistling)*
| Not botherin' nobody only keeping your head down
| *(whistling)*

| You see a friendly neighbor selling jewelry on the cheap
| You make an easy bargain never knowing you're in deep
| Next day the guard appears and then they throw you in the keep
| *(whistling)*
