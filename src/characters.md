---
title: "Curse of the Red Mill"
---

## Characters and Roles

"Curse of the Red Mill" calls for a principal cast of 7 and a Chorus of 3-6. Elandra, Sheriff, and Baron all begin the show as part of the chorus as well, until their characters are named.

Pronouns of the characters are listed, but actors of any gender are welcome to audition for any role. Additionally, broad vocal ranges (soprano/alto/tenor/bass) are listed, but the music director is more than happy to adjust keys or parts to fit an actor's range as desired.

### The Game Group

- **The Game Master (GM)** (he/him, bass) - assertive, precise, and combative. Trying to run a no-nonsense dungeon crawl, but the players have other ideas. He believes that the players look to him to guide them, and takes the game and its rules and prep seriously.
- **Gwendolyn (Gwen)** (she/her, soprano, lawful good) - naive, cheery, dramatic, strong-willed. Daughter of the Sheriff, niece of Elandra, engaged to marry the Baron tomorrow against her will. Doesn't want to marry, does want freedom and to adventure. Gwen's player is a gentle, outgoing peacemaker. She sees herself as the facilitator, introducing her friends to the game and dealing with any personality clashes by distracting and refocusing. She is big into character drama and exploring emotional spectrums.
- **Bernadette** (she/her, alto, neutral good) - swashbuckling, competent, heroic, but with an edge of danger. Friend of Gwen, captain of the Arachne. Wants to help her friend Gwen to escape from her life and go adventuring together. Bernadette's player is curious, analytical, and introverted. She is new to the game, and insecure about it, which she masks with bravado. But when acting, her insecurity and introversion disappears, replaced by confidence. She's most interested in playing a big pulpy hero.
- **Jax** (they/them, tenor, chaotic neutral) - trickster, unpredictable, a bit selfish and pragmatic, but charismatic. Con artist, no money, and on the run from the Baron. Hiding out in the inn until they get kicked out. Jax's player is joyful, creative, and sociable. They thrive as the center of attention, and will resort to stirring the pot if needed, just to get a reaction. They're easily bored and distracted, and want to do something that nobody expects.

### NPCs

- **Elandra** (she/her) - Gwen's Aunt, sister of the Sheriff, and owner of the famous Red Mill Inn. Secretly also the monster of the mill, transforming at night into a terrible beast until someone shows her love while in that form. She wants Gwen to be free, which is why she hired her at the inn. She has given up hope of having much of a life for herself.
- **Sheriff** (she/her) - Gwen's mother, Elandra's sister. Is dead-set on getting Gwen married to the Baron, primarily because it will improve her own status. No-nonsense narcissistic lady, who is not too bright and not at all used to being defied. Naturally, does not get along with Bernadette or Jax (or most anybody).
- **Baron** (he/him) - pompous, womanizing, overblown goof who is the embodiment of unearned privilege. Used to being in charge of everyone and getting whatever he wants.

### Additional Roles

- **Chorus** - plays the roles of every other NPC in the story, including: patrons of the inn, monsters of the dungeon, servants of the Baron, and guests at the wedding. There are few lines, but several songs and lots of on-stage action.
- **Thrulsnard the Barbarian** - a barbarian whose player was working and couldn't make it to the game. Usually seething with silent (or not-so-silent) rage. Thrulsnard appears on stage less frequently than the other roles, and has few lines, but sometimes sings with the chorus.
