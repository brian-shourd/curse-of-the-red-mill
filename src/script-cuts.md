---
title: "Curse of the Red Mill"
---

## Script Excerpts

These are a few excerpts from the script which we will use for readings during the audition. Don't try to memorize them or anything, these are just provided so that you can be acquainted with them before the audition, if you want.

- [Scene 1: GM/Players](#scene-1)
- [Scene 2: Sheriff/Jax/Bernadette](#scene-2)
- [Scene 3: Gwen/Bernadette](#scene-3)

### Scene 1: GM/Players {#scene-1}

This excerpt is from early on, before the gaming group has settled down to play. Just before the scene begins, Jax and Bernadette have been arguing, and Gwen is attempting to distract them so that the game can get back on track.

**GWEN:** *(brandishing a CD)* Wait, wait! I almost forgot! I brought a new player, so I get to pick the soundtrack today, right?

**GM:** What? Oh, yeah, that is the rule, isn't it? Ok, fine, you can be the DJ tonight. But it better be epic!

**GWEN:** Oh, it'll be perfect! Just what we need to give this game a little pizzaz! It's got something for everyone - a little adventure, some romance. *(to GM)* I made it just for you.

**GM:** *(oblivious)* For me? I don't like romantic music. Anyway, before we get sidetracked again, I did ask you all to come up with character backstories and reasons that they come here, to the inn near the Red Mill.

**BERNADETTE:** What's the-

**GM:** *(hamming it up)* What's the Red Mill, you ask?

*(As GM speaks, CHORUS NPCs and ELANDRA enter and set the stage for the inn. They remain on stage as inn patrons, seating at tables or the bar, silently ordering drinks, conversing, or playing dice.)*

Well, for years the famous Red Mill of this town has been plagued by a legendary and monstrous Beast, and adventurers from around the world travel to test their might against it. Many have tried, and many have died, seeking the heart of the cursed creature who stalks the tower of the mill each night. And by tradition, these brave souls all spend a night or two here, at this inn, where the innkeeper Elandra hears their tales of past glories before they go to meet their fate against the beast.

Let's introduce-

**BERNADETTE:** Who's at the inn right now?

**GM:** What?

**BERNADETTE:** You're setting the scene, right? Who all is there?

**GM:** I don't know, some villagers. It's not important.

**BERNADETTE:** No, it is. *(pointing at an NPC)* Who's that? *(teasing GWEN)* He's got a nice aura.

*(MARVIN mouths to GM "what's my name?")*

**GM:** Uh, that's, uh, Mar...vin. Marvin.

**NPCs:** *(raising hands in celebration)* Marvin!

**BERNADETTE:** What's he do?

**GM:** He's a.... milliner.

**BERNADETTE:** Marvin the Marvelous Milliner?

**JAX:** He works in the cursed mill?

**GWEN:** No, that's a miller.

**JAX:** But Marvin's a...?

**GM:** Milliner.

*(beat, JAX is puzzled)*

**BERNADETTE:** You know, a modiste?

*(beat)*

**GWEN:** Like a haberdasher.

**JAX:** Did you three go equal shares on a word-of-the-day calendar? What are you talking about?

**GM:** A hatter. He makes hats.

**JAX:** Just say that, then!

**BERNADETTE:** Why does this little village need a dedicated milliner? Do they all wear elaborate hats?

*(simultaneously)*
**GM:** No. **JAX:** Yes!

**JAX:** Too late, I'm imagining all of them with exquisite headpieces now!

**GM:** But that's goofy! It'll ruin the atmosphere!

**BERNADETTE:** I kinda like it.

**GWEN:** Me too.

*(All NPCs pull out unlikely hats concealed nearby and don them. For the remainder of the show, every NPC wears a hat.)*

**GM:** Whatever, it doesn't matter anyway. Now before we get even more distracted, can we please introduce your characters and their backstories? Briefly? Gwendolyn, start us off.

**GWEN:** Yes, sir!

*(GWEN hits play on the boom box. GM groans as the music starts)*

### Scene 2: Sheriff/Jax/Bernadette {#scene-2}

In this scene, Jax and Bernadette are disguised as Sherlock Holmes and Dr. Watson in order to fool the Sheriff. They have just introduced themselves and established their credentials.

**SHERIFF:** Very well, I have just one question for you, then-

**BERNADETTE:** I'll be asking the questions, here. When was Gwendolyn last seen?

**SHERIFF:** Last I saw her she was safely in the jail cell at the station.

**BERNADETTE:** In the jail cell? What crime had she committed?

**SHERIFF:** She was planning to run away before her wedding?

**BERNADETTE:** Eloping, you mean?

**SHERIFF:** Escaping, more like.

**JAX:** Aha! I have made an important deduction, Watson, write this down! The bride did not wish to be wed! She was being coerced!

**BERNADETTE:** But why, Holmes? Why wouldn't she wish to wed the baron?

**SHERIFF:** Because she is a willful child!

**JAX:** An intriguing deduction - for an amateur. If she is so willful then it must have been quite the ordeal to get her in the cell in the first place.

**SHERIFF:** Not at all, she went of her own accord.

**BERNADETTE:** She went willingly?

**SHERIFF:** Well-

**BERNADETTE:** And I remind you that you are under oath!

**SHERIFF:** I am?

**JAX:** Please answer the question.

**SHERIFF:** Wait, am I under investigation?

**JAX:** Doctor Watson, advise your witness to answer the questions asked or I will find her in contempt of court!

**BERNADETTE:** Yes, your honor. I ask you again, Sheriff - did she go to jail willingly?

**SHERIFF:** Not exactly.

**BERNADETTE:** Permission to treat the witness as hostile?

**JAX:** Granted.

**BERNADETTE:** You threw her in the cell by force, didn't you?!

**SHERIFF:** No! I only threatened her friends. And only a little.

**JAX:** *(channeling Judge Judy)* Oh, you only threatened her friends...

**BERNADETTE:** And these friends - they broke her out?

**SHERIFF:** Those gormless fools? I might have thought so, but it happens that I was with them when she escaped. I'm sure that they had something to do with it, though - you should be questioning them!

**JAX:** I have made a second deduction, Doctor Watson!

**BERNADETTE:** And that is?

**JAX:** I deduce that Gwendolyn has very good friends.

**BERNADETTE:** Clearly the best, Holmes.

**SHERIFF:** I object to that.

**JAX:** Overruled.

**BERNADETTE:** *(impersonating Columbo)* And one more thing - this baron that you arranged for your daughter to marry...

**SHERIFF:** Yes?

**BERNADETTE:** He's a good man? Nice fellow? The sort of guy that a woman would like to wed? Not at all some kind of monstrous, overbearing ogre?

**SHERIFF:** I don't see what that has to do with it. He's a baron - that I was able to arrange this wedding at all is a blessing on our house. Gwendolyn should be happy to be able to do something for her future family.

**BERNADETTE:** And her current mother?

**SHERIFF:** So it benefits me, too, what of it?

**BERNADETTE:** *(turning to the audience)* Esteemed members of the jury-

### Scene 3: Bernadette/Gwen {#scene-3}

In this scene, Gwen has sent a letter to her old friend Bernadette, asking for unspecified help about her unwanted wedding to the baron. Bernadette has been asking Gwen for years to sail away with her so that they can adventure together and believes that Gwen is finally ready to go.

**GWEN:** Bernadette! You came!

*(GWEN runs over to embrace BERNADETTE, casually stepping over CROWLEY)*

**BERNADETTE:** I got your letter! You said to come at once, and as your best and oldest friend, I loosed all sails to be here. I didn't miss the wedding, did I?

**GWEN:** Not yet, but if we hurry we can both miss it! You still have your ship?

**BERNADETTE:** Until my dying day.

*(JAX hears this and surreptitiously slides closer to be able to hear better)*

**GWEN:** Good. *(pausing awkwardly)* Please -

*(GWEN gestures for BERNADETTE to sit with her. They sit. GWEN fidgets nervously, but ELANDRA catches her eye, smiles, and GWEN relaxes. ELANDRA nods and leaves them alone, going to help other patrons at the inn. She directs some patrons to drag CROWLEY off the stage.)*

**GWEN:** I need to ask you for a big favor.

**BERNADETTE:** Is that all? Don't worry, Elandra was just telling me - you only ever needed to ask.

**GWEN:** Really?

**BERNADETTE:** Don't act so surprised! What are world-renowned sailing friends for? It'll be nice to have a new face on the ship.

**GWEN:** *(nervous and a little sickened)* Oh. So... do you need any... special supplies?

**BERNADETTE:** Just the usual provisions. It won't hold us up any, if that's what you're asking.

**GWEN:** I mean, for the *(lowers voice conspiratorially)* kidnapping?

**BERNADETTE:** *(puzzled)* You want to stage it as a kidnapping? I don't know, that sounds messy...

**GWEN:** *(horrified)* You weren't planning to kill him, were you?

**BERNADETTE:** Woah, woah, woah - you mean the baron!? What kind of rumors are they spreading about me around here? I'm not killing or kidnapping anyone - definitely not a baron! I thought that you were finally going to come adventuring with me, like we've always dreamed!

**GWEN:** But you know I can't do that! If I left, I'd never hear the end of it from my mother!

**BERNADETTE:** Luckily, she's not invited!

