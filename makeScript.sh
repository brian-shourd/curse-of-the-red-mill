#!/bin/bash

set -euo pipefail

if [ -z "${COTRM_SRC_DIR}" ]; then
  echo "Unable to finde COTRM_SRC_DIR; please set it and continue";
  exit 1;
fi

FILE_LIST=(
  "./src/script-header-file.md"
  # Act 1
  # 1 'Round This Table 202402241103
  "${COTRM_SRC_DIR}/202402241103.md"
  # 2 Gwendolyn 202402251624
  "${COTRM_SRC_DIR}/202402251624.md"
  # 3 Bernadette 202402241109
  "${COTRM_SRC_DIR}/202402241109.md"
  # 4 Jax 202402241156
  "${COTRM_SRC_DIR}/202402241156.md"
  # 6 Adventure of Our Dreams 202402252021
  "${COTRM_SRC_DIR}/202402252021.md"
  # 7 Go While the Goin is Good 202403141548
  "${COTRM_SRC_DIR}/202403141548.md"
  # 8 You Never Can Tell 202403141551
  "${COTRM_SRC_DIR}/202403141551.md"
  # 9 Arrest 202402271857
  "${COTRM_SRC_DIR}/202406091240.md"
  # 10 Lest My Family 202402281546
  "${COTRM_SRC_DIR}/202402281546.md"
  # 12 You Belong to You 202403121029
  "${COTRM_SRC_DIR}/202403121029.md"
  # Act 2
  # 14 Idle Boasting 202403131311
  "${COTRM_SRC_DIR}/202403131311.md"
  # 15 Legend of the Mill 202403121556
  "${COTRM_SRC_DIR}/202403121556.md"
  # 17 When You're a Bard 202403060848
  "${COTRM_SRC_DIR}/202403060848.md"
  # 18 Love Is 202403131305
  "${COTRM_SRC_DIR}/202403131305.md"
  # 19 Elementary 202402232121
  "${COTRM_SRC_DIR}/202402232121.md"
  # 20 Every Day Is Ladies Day With Me 202403161528
  "${COTRM_SRC_DIR}/202403161528.md"
  # 21 If You Love But Me 202403141259
  "${COTRM_SRC_DIR}/202403141259.md"
  # 24 Entrance of the Wedding Guests 202403140755
  "${COTRM_SRC_DIR}/202403140755.md"
  # Credits
  "${COTRM_SRC_DIR}/202404011123.md"
)
ASSEMBLED_MD="./src/script.md"

clean_file() {
  # Remove yaml frontmatter, and remove Links
  # echo '<div class="page-break"></div>'
  tail -n +2 \
    | sed -n '/^---$/,$p' \
    | tail -n +2 \
    | sed '/^### Links$/,$d'
}

for file in ${FILE_LIST[@]}; do
  if [ ! -f "${file}" ]; then
    echo "Unable to find file ${file}"
    exit 1
  fi
done

cat "${FILE_LIST[0]}" > "${ASSEMBLED_MD}"

for file in ${FILE_LIST[@]:1}; do
  cat "${file}" | clean_file >> "${ASSEMBLED_MD}"
done

